#include<iostream>
#include<algorithm>
#include "f4v6.h"
#include "output.h"

int basis_length(const List_poly &G, const vector<bool> &ncsry)
{
    int count = 0;
    for(int i = 0; i < G.size(); i++)
        if(ncsry[i])
            count++;
    return count;
}
int basis_length(const vector<List_poly> &GG, const vector<vector<bool> > &ncsry)
{
    int count = 0;
    for(unsigned int step = 0; step < GG.size(); step++)
        count += basis_length(GG[step], ncsry[step]);
    return count;
}
int queue_length(const vector<list<Poly_poly>*> &BB)
{
    int count = 0;
    for(unsigned short deg = 0; deg < BB.size(); deg++)
    {
        if(BB[deg])
            count += BB[deg]->size();
    }
    return count;
}
ostream& operator<<(ostream& out, const Poly_poly &p)
{
    out<<"(F_"<<p.step1
        <<"["<<p.i_f1
        <<"],F_"<<p.step2
        <<"["<<p.i_f2<<"])";
    return out;
}
ostream& operator<<(ostream& out, const Signature &sig)
{
    out<<*to_monomial(sig.t)
        <<"*F_"<<sig.step
        <<"["<<sig.i_f
        <<"]="<<*to_monomial(sig.lt);
    return out;
}
ostream& operator<<(ostream& out, const list<Poly_poly> &P)
{
    if( P.size() == 0)
        out<<"empty";
    else
    {
        out<<P.size()<<" -> ";
        list<Poly_poly>::const_iterator iter = P.begin();
        while(true)
        {
            out<<"(F_"<<iter->step1
                <<"["<<iter->i_f1
                <<"],F_"<<iter->step2
                <<"["<<iter->i_f2<<"])";
            //prepaper for next pair
            iter++;
            if(iter == P.end())
                break;
            else
                out<<", ";
        }
    }
    return out;
}
ostream& operator<<(ostream& out, const vector<list<Poly_poly>*> &BB)
{
    unsigned short d = 0;    
    out<<"Pairs:"<<endl;
    while(true)
    {
        if( BB[d] && BB[d]->size() != 0)
        {
            //print degree d pairs
            out<<"\td = "<<d<<": ";
            list<Poly_poly>::const_iterator iter = BB[d]->begin();
            while(true)
            {
                out<<"(F_"<<iter->step1
                    <<"["<<iter->i_f1
                    <<"],F_"<<iter->step2
                    <<"["<<iter->i_f2<<"])";
                //prepaper for next pair
                iter++;
                if(iter == BB[d]->end())
                    break;
                else
                    out<<", ";
            }
        }
        //prepare for next degree
        d++;
        if(d >= BB.size())
            break;
        else if(BB[d])
            out<<endl;
    }
    return out;
}

void F4_test(const List_poly &F,  //< input sequence
             List_poly &G	     //< output GB of <F>. 
             )
{
#ifdef PRINT_VERBOSE
    short step = 0;
#endif
    //Start F4
    vector<list<Signature> > HH;                 //< Stores signatures of H after symbolic preprocessing on each step
    vector<List_poly> GG;                       //< Stores Htilde after row reduction on each step
	vector<list<Poly_poly>*> BB(MAX_DEG, (list<Poly_poly>*)NULL); //< Stores all unprocessed pairs discriminated by degree
    vector<vector<bool> > ncsry;                 //< ncsry[step][i]==true iff the i-th poly in GG[step] is necessary for making pairs
    //reserve space for HH GG and unnecessary
    HH.reserve(MAX_STEP);
    GG.reserve(MAX_STEP);
    ncsry.reserve(MAX_STEP);
    //step zero
    HH.push_back(list<Signature>());            //< HH[0] = \empty
    GG.push_back(F);                            //< GG[0] = F
    ncsry.push_back(vector<bool>(F.size(),true)); //< create necessary vector for new elts F. All true
    update(GG, &BB, &ncsry);
    //prepare for step 1
	short deg = select(BB);            //< degree of the step
    Monomial::increase_indexed(deg);   //< indexes monomials up to degree deg
    while( deg != -1 && GG.size() <= MAX_STEP)  //< while there are still pairs in BB and not too many steps
	{
#if defined (PRINT_VERBOSE) || defined (PRINT_TIME) || defined(PRINT_MEMORY)
        fout<<endl;
#endif
#ifdef PRINT_MEMORY
        PROCESS_MEMORY_COUNTERS pmc;
        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
#ifdef PRINT_VERBOSE
        step++;
        fout<<step
            <<'\t'<<basis_length(GG, ncsry)
            <<'\t'<<queue_length(BB)
            <<'\t'<<deg
            <<'\t'<<BB[deg]->size();
#endif
#if defined (PRINT_VERBOSE) && defined (PRINT_TIME)
        fout<<'\t';
#endif
        list<Signature> L;      //< A list of monomial-polynomial pairs
#ifdef PRINT_DEBUG
        fout<<endl<<"G: ";
        for(unsigned int i = 0; i < GG.size(); i++)
        {
            fout<<endl<<"\tfrom step "<<i<<endl;
            print_ncsry(fout, GG[i], ncsry[i]);
        }
        fout<<endl<<BB;
#endif
#ifdef PRINT_TIME
        clock_t timer1 = clock();
#endif
		left_right(*(BB[deg]), GG, &L);
#ifdef PRINT_TIME
        fout<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
#endif
#ifdef PRINT_MEMORY
        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
#ifdef PRINT_DEBUG
        fout<<endl<<"H after left_right: "<<endl<<H;
#endif
        delete BB[deg]; BB[deg] = NULL; //<delete the pairs used
        List_poly H(deg);       //< A list of polynomials of degree up to deg
#ifdef PRINT_TIME
        timer1 = clock();
#endif
        symbolic_preprocessing(GG, ncsry, HH, &H, &L);
#ifdef PRINT_TIME
        fout<<'\t'<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
#endif
#ifdef PRINT_MEMORY
        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
#ifdef PRINT_DEBUG
       fout<<endl<<"H after Symbolic: "<<endl<<H;
#endif
        HH.push_back(L);    //< signatures of H
        L.clear();
        //make a copy of the leading monomials of H before gauss
        set<Monomial::Index> lead_monomial_H(H.lead_monomial_begin(), H.lead_monomial_end());
#ifdef PRINT_VERBOSE
        int nnz_p = H.nnz_polys(),
            nnz_t = H.nnz_monomials(),
            nnz_c = H.nnz_coefs();
        fout<<'\t'<<nnz_p
            <<'\t'<<nnz_t
//            <<'\t'<<(double)nnz_c / nnz_p / nnz_t * 100
            <<'\t'<<nnz_c;
//        cout<<endl<<nnz_p<<'\t'<<nnz_t<<'\t'<<nnz_c;
#endif
#ifdef PRINT_TIME
        timer1 = clock();
#endif
		reduction_row_echelon(&H);
#ifdef PRINT_TIME
        fout<<'\t'<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
#endif
#ifdef PRINT_MEMORY
//        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
//            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
#ifdef PRINT_VERBOSE
        nnz_p = H.nnz_polys();
        nnz_t = H.nnz_monomials();
        nnz_c = H.nnz_coefs();
        fout
//            <<'\t'<<(double)nnz_c / nnz_p / nnz_t * 100
            <<'\t'<<nnz_c;
#endif
#ifdef PRINT_DEBUG
        fout<<endl<<"H after echelon: "<<endl<<H;
#endif
        GG.push_back(H);    //< store permanently H in GG.
#ifdef PRINT_MEMORY
        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
#ifdef PRINT_DEBUG
        fout<<endl<<"copied to G: "<<endl<<GG.back();
#endif
        H.clear();
        ncsry.push_back(vector<bool>(GG.back().size(),true)); //< create necessary vector for new elts H
        //mark as unnecessary if leading monomial appears in the leading monomials of H before gauss
        remove_if_LM_in(GG.back(), &(ncsry.back()), lead_monomial_H);
#ifdef PRINT_VERBOSE
        fout<<'\t'<<basis_length(GG.back(), ncsry.back());
#endif
        lead_monomial_H.clear();
        //prepare for next step
#ifdef PRINT_TIME
        timer1 = clock();
#endif
        update(GG, &BB, &ncsry);
#ifdef PRINT_TIME
        fout<<'\t'<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
#endif
#ifdef PRINT_MEMORY
        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
        deg = select(BB);
#ifdef PRINT_TIME
        timer1 = clock();
#endif
        Monomial::increase_indexed(deg);   //< indexes monomials up to degree deg
#ifdef PRINT_TIME
        fout<<'\t'<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
#endif
#ifdef PRINT_MEMORY
        if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
            fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
	}
    //Prepare Output. Move necessary to G
    deg = 0;
    for(unsigned short s = 0; s < GG.size(); s++)
        deg = MAX( deg, GG[s].degree() );
    G.reset(deg);
    for(unsigned short s = 0; s < GG.size(); s++)
    {
        for(int i_f = 0; i_f < GG[s].size(); i_f++)
        {
            if(ncsry[s][i_f])
            {
                int i_g = G.new_poly();
                multiply(0, GG[s], i_f, &G, i_g);
            }
        }
    }
}
