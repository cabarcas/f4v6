/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file list_poly_io.cpp
    Implementation of the List_poly class I/O functions.
*/
#include<sstream>
#include<fstream>
#include<iostream>
#include"List_poly.h"

Monomial::Index str_to_monomial(const char *s)
{
    Monomial t;
    if(s[0] == '1')
        return 0;
    if(s[0] != 'z')
    {
        cout<<"error, bad monomial: "<<s;
        exit(1);
    }
    //default case
    istringstream sin(s);
    short exp;  //< exponent
    short var;  //< variable
    char op;    //< operator
    while( !sin.eof() )
    {
        sin.ignore(2); //< ignores "z["
        sin>>var; // reads exponent
        sin.ignore(1); //< ignores "]"
        //get exponent
        exp = 1;    //< deafault exponent 1
        op = sin.peek();
        if(op == '^')
        {
            sin.ignore();   //ignores "^"
            sin>>exp;
        }
        t[var-1] = exp;
        if(op == '*')
            sin.ignore();   //ignores "*"
    }
    return t.to_index();
}

Field powOfx(const int e)
{
	Field pow(1);
	for(int i = 0; i < e; i++)
		pow *= 2;
	return pow;
}
void read_magma(const char *file_name, List_poly &F)
{
    ifstream fin(file_name);
    int m;  //< Number of polynomials
    int n;  //< Number of monomials
    int i;  //< index of polynomial
    int j;  //< index of monomial
    int index_monomial; //< index of monomial in all_monomials
    unsigned short exp;        //< exponent of non-zero coef
    char s[20];
    // read number of polynomials
    fin>>m;
    i = F.new_poly(m);
    // read monomials
    fin>>n;
    vector<Monomial::Index> all_monomials;
    all_monomials.reserve(n);
    for(j = 0; j < n; j++)
    {
        fin>>s;
        all_monomials.push_back(str_to_monomial(s));
    }
    // read coefs
    for( i = 0; i < m; i++)
    {
        fin>>n;
        for(j = 0; j < n; j++)
        {
            fin>>index_monomial;
            fin>>exp;
            F.set_coef(i, all_monomials[index_monomial-1], powOfx(exp));
        }
    }
}
ostream& operator<<(ostream& out, const Monomial &t)
{
    Monomial::const_iterator var = t.begin();
    if(var == t.end())
        out<<"1";
    else
    {
        while(true)
        {
            //out<<"z["<<var->first+1<<"]";
            out<<"x_"<<var->first+1;
            if(var->second > 1)
                out<<"^"<<var->second;
            var++;
            if(var == t.end())
                break;
            else
                out<<"*";
        }
    }
    return out;
}

ostream& operator<<(ostream& out, const List_poly &F)
{
    if(F.size() == 0)
        out<<"empty";
    else
    {
        Field *aux = new Field [F.coef.n_cols()];
        int i = 0, j;
        while(true)
	    {
            out<<'\t';
            //initialize aux with zeros
            for(j = 0; j < F.coef.n_cols(); j++)
                aux[j] = 0;
            //copy coefs to aux
            int max_j = full_size_row(F.coef, i, aux);
            if(max_j < 0)
                out<<0;
            else
            {                
                //print in order non-zero
                j = 0;
                bool no_plus = true;
                while(true)
                {                    
                    if(aux[j] != 0)
                    {
                        if(aux[j] != 1)
                        {
                            out<<"a^{"<<aux[j].log()<<"}";
                            out<<'*';
                        }
                        else
                        {
                            out<<"1*";
                        }
                        out<<*to_monomial(F.col_to_monomial(j));
                        no_plus = true;
                    }
                    j++;
                    if(j > max_j)
                        break;
                    else if(no_plus)
                    {
                        no_plus = false;
                        out<<" + ";
                    }
                }
            }
            i++;
            if(i >= F.m_poly)
                break;
            else
		        out<<endl;
        }
        delete aux;
    }
    return out;
}

void print_ncsry(ostream& out, const List_poly &F, vector<bool>& ncsry)
{
    if(F.size() == 0)
        out<<"empty";
    else
    {
        Field *aux = new Field [F.coef.n_cols()];
        int i = 0, j;
        while(true)
	    {
            out<<"\t("<<ncsry[i]<<")";
            //initialize aux with zeros
            for(j = 0; j < F.coef.n_cols(); j++)
                aux[j] = 0;
            //copy coefs to aux
            int max_j = full_size_row(F.coef, i, aux);
            if(max_j < 0)
                out<<0;
            else
            {                
                //print in order non-zero
                j = 0;
                bool no_plus = true;
                while(true)
                {                    
                    if(aux[j] != 0)
                    {
                        if(aux[j] != 1)
                        {
                            out<<"a^{"<<aux[j].log()<<"}";
                            out<<'*';
                        }
                        else
                        {
                            out<<"1*";
                        }
                        out<<*to_monomial(F.col_to_monomial(j));
                        no_plus = true;
                    }
                    j++;
                    if(j > max_j)
                        break;
                    else if(no_plus)
                    {
                        no_plus = false;
                        out<<" + ";
                    }
                }
            }
            i++;
            if(i >= F.m_poly)
                break;
            else
		        out<<endl;
        }
        delete aux;
    }
}
