/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file monomial_static.cpp
    Implementation of monomial class static member functions.
*/
#include "monomial.h"

/** \file monomial_static.cpp
    \brief Implements static members of Monomial for the general case of K[x_1,...,x_n]
*/
//-----------------------------------------------------------------------------
//---------------------------------Conversion functions------------------------
//-----------------------------------------------------------------------------
/// returns the corresponding index, or -1 if not indexed
Monomial::Index Monomial::to_index()
{
    short d = degree(*this);
    if(d > cur_deg)
        return -1;
    //binary search
    bool found = false;
    int low = d<=0 ? 0 : deg_sep[d-1]+1,
        high = deg_sep[d],
        mid;
    while(!found && low <= high)
    {
        mid = (low+high)/2;
        short c = compare(*this, indx_monomials[mid]);
        if(c == 0)
            return mid;
        else if(c < 0)
            high = mid - 1;
        else
            low = mid +1;
    }
    return -1;
}
//-----------------------------------------------------------------------------
//---------------------------------Static functions----------------------------
//-----------------------------------------------------------------------------
short Monomial::num_var = 1;
short Monomial::cur_deg = 0;                    ///< maximum degree indexed so far
vector<Monomial> Monomial::indx_monomials(1,Monomial());    ///< Indexed monomials. all the monomials up to certain degree in ascending order acording to the given order
vector<Monomial::Index> Monomial::mult_table;       ///< multiplication table. x_i * indx_monomials[j] = indx_monomials[mult_table[i + num_var*j]]
vector<Monomial::Index> Monomial::deg_sep(1,0);     ///< Degree separations on indx_monomials. deg_sep[d] is the last index of a monomial of degree d

/// indexes monomials up to degree deg
/** If deg > cur_deg indexes all the monomials up to degree deg.
    else it takes no action.

    Monomials are stored in the vector indx_monomials according to the given order from lowest to higher.
*/
void Monomial::increase_indexed(const short deg)
{
    if(deg > cur_deg)
    {
        deg_sep.resize(deg+1);
        int next_index = indx_monomials.size(); //< next index to store a monomial
        //efficient computation of next combinatorial coeficient
        /* C(n+d+1, n) - C(n+d, n) = C(n+d, d+1) = C(n+d, n) * n / (d+1)
            hence
            C(n+d+1, n) = C(n+d, n) + C(n+d, n) * n / (d+1)
            or said in a more appropiate way
            C(n+d, n) = C(n+d-1, n) + C(n+d-1, n) * n / d
        */
        int increased_size = next_index; // C(n+d, n)
        int d;                      //< degree
        for (d = cur_deg+1; d <= deg; d++)
        {
            increased_size += increased_size * Monomial::num_var / d;
            deg_sep[d] = increased_size - 1;
        }
        indx_monomials.resize(increased_size);
        mult_table.resize((deg_sep[deg - 1]+1) * Monomial::num_var);

        Monomial uu;                    //< output monomial

        short s;                    //< sum of degrees
        short var;                  //< index of a variable to be changed
        Monomial::iterator iter;        //< iterator to a var in uu

        // Special case for first time around, degree zero
        if(cur_deg < 0 && deg > -1)
        {
            indx_monomials[next_index] = Monomial();
            next_index++;
            cur_deg = 0;
        }
        for (d = cur_deg+1; d <= deg; d++)
        {
            //initialize uu to 0
            uu.clear();
            //accomodate d as far right as possible
            uu[Monomial::num_var-1] = d;
            while(true)     //< while there are more divisors of this degree
            {
                //-----------------------------------------------------
                //-------------------------------Use uu here
                //-----------------------------------------------------
                indx_monomials[next_index] = uu;
                //fill mult_table entries
                for(iter = uu.begin(); iter != uu.end(); iter++)
                {
                    Monomial x;
                    x[iter->first] = 1;//< variable
                    Monomial::Index j = quo(uu, x).to_index();  //divide by the variable and get the index
                    mult_table[iter->first + Monomial::num_var*j] = next_index; //x_i * indx_monomials[j] = indx_monomials[mult_table[i + num_var*j]]
                }
                next_index++;
                //-----------------------------------------------------
                //-------------------------------Serch for next monomial
                //-----------------------------------------------------
                //if last element in this degree
                if(     d == 0 || 
                        (d > 0 && uu.begin()->first == 0 && uu.begin()->second == d))
                    break;
                //set iter to last non-zero
                iter = uu.end();
                iter--;
                var = iter->first; //< store the variable correspondong to iter
                //add one to previous variable and leave iter in the same place
                if(iter == uu.begin())
                    uu[var-1] = 1;   //< inserts entry
                else
                {
                    iter--;
                    if(iter->first == var-1)//< if this variable has non-zero exponent
                        iter->second++;
                    else
                    {
                        uu[var-1] = 1;   //< inserts entry
                        iter++;
                    }
                    iter++;
                }
                //clear from there on and compute sum -1
                s = -1;
                while( iter != uu.end() )
                {
                    s += iter->second;
					Monomial::iterator temp = iter;
					iter++;
                    uu.erase(temp);
                }
                //accomodate s as far right as possible
                if(s > 0)
                    uu[Monomial::num_var-1] = s;
            }//while more monomials of degree d
            cur_deg = d;
        }//for d        
    }//if deg >= cur_deg
}
//-----------------------------------------------------------------------------
//---------------------------------divisor generation--------------------------
//-----------------------------------------------------------------------------
//Finds the first divisor of t in glex order.
void first_divisor(const Monomial &t, Monomial *u, short *d)
{
    *u = Monomial();
    *d = 0;
}
/// Finds next divisor of t.
/** Given that u is a divisor of t, rewrites the value to u
    to the next divisor in glex order and stores the degree
    in d.
*/
bool next_divisor(const Monomial &t, Monomial *u, short *d)
{
    short s,    //< stores some sum to be decremented
        min,    //< stores some exponent minimun between two.
        d_t;    //< degree of t
    Monomial::const_iterator i_t;
    //if u is the trivial divisor 1
    if(u->size() == 0)
    {
        if(t.size() > 0) //< if t is the trivial monomial 1
        {
            // makes the last var's exp 1 and returns
            i_t = t.end();
            i_t--;
            (*u)[i_t->first] = 1; 
            *d = 1;
            return true;
        }
        return false;
    }
    *d = degree(*u); //< Find the degree of u
    //set i_u to last non-zero variable
    Monomial::iterator i_u = u->end();  //< An iterator to a var of u
    i_u--;
    //Find the corresponding position in t
    i_t = t.find(i_u->first);
    //If first variable
    if(i_t == t.begin())
    {        
        d_t = degree(t);
        if(*d == d_t)
            return false;
        //end of degree
        *d = (*d)+1;
        //clear u and accomodate d as far right as possible
        u->clear();
        s = *d;
        i_t = t.end();
        for(i_t--; s > 0; i_t--)
        {
            min = MIN(i_t->second, s);
            (*u)[i_t->first] = min;
            s -= min;
        }
        return true;
    }
    //add one to previous not full variable and leave i_u in next position
    i_t--;
    if(i_u == u->begin())
        (*u)[i_t->first] = 1;
    else
    {
        //search for second right-most not full
        i_u--;
        while( i_u != u->begin() &&     //< i_u is not first
        i_u->first == i_t->first && //< same variable
        i_u->second == i_t->second )//< same exponent
        {
            i_u--;
            i_t--;
        }
        //Conditon for no more divisors of this degree
        if( i_u == u->begin() &&        //< i_u is first
            i_t == t.begin() &&         //< i_t is first
            i_u->first == i_t->first && //< same variable
            i_u->second == i_t->second )//< same exponent
        {
            d_t = degree(t);
            if(*d == d_t)
                return false;
            //end of degree case
            *d = (*d)+1;
            //clear u and accomodate d as far right as possible
            u->clear();
            s = *d;
            i_t = t.end();
            for(i_t--; s > 0; i_t--)
            {
                min = MIN(i_t->second, s);
                (*u)[i_t->first] = min;
                s -= min;
            }
            return true;
        }
        //else i_u not first or i_t not first or different variable or different exponent
        if(i_u == u->begin() //< i_u is first
            && i_u->first == i_t->first //< same variable
            && i_u->second == i_t->second) //< same exponent hence i_t not first
        {
            i_t--;
            (*u)[i_t->first] = 1;
        }
        else if( (i_u == u->begin() //< i_u is first
            && i_u->first == i_t->first //< same variable
            )//< but different exponent
            || ( i_u != u->begin() //< i_u not first
            && i_u->first == i_t->first ) ) //< same variable
        {
            i_u->second++;
            i_u++;
        }
        else if(i_u == u->begin() //< i_u is first
            && i_u->first != i_t->first)//< Different variable
        {
            (*u)[i_t->first] = 1;
            i_u++;
            i_u++;
        }
        else//< i_u not first and different variable
        {
            (*u)[i_t->first] = 1;
        }
    }
    //clear from there on and compute sum -1
    s = -1;
    while( i_u != u->end() )
    {
        s += i_u->second;
		Monomial::iterator temp = i_u;
		i_u++;
        u->erase(temp);
    }
    //accomodate s as far right as possible
    i_t = t.end();
    for(i_t--; s > 0; i_t--)
    {
        min = MIN(i_t->second, s);
        (*u)[i_t->first] = min;
        s -= min;
    }
    return true;
}
