//Magma program fot printing to a file in appropiate format.
write_polys_formated := procedure(file, F)	
	//Print Number of polynomials
	Write(file, #F);
	//find terms and coefs
	terms := [Monomials(f): f in F];
	coefs := [Coefficients(f): f in F];
	//group all terms
	all_terms := {};
	for L in terms do
		for a in L do
			Include(~all_terms, a);
		end for;
	end for;
	//sort terms
	all_terms := Sort([a: a in all_terms]);
	Reverse(~all_terms);
	//print terms	
	Write(file, #all_terms);
	for i in [1..#all_terms] do
		Write(file, all_terms[i]);
	end for;
	//print coefs
	for i in [1..#terms] do
		Write(file, #terms[i]);
		for j in [1..#terms[i]] do
			Write( file, Index(all_terms, terms[i][j]) );
			Write( file, Log(coefs[i][j]) );
		end for;
	end for;
//Write(file, F);
//Write(file, terms);
end procedure;
