/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file GFpow.h
    Declaration of the GFpow class functions.
*/
#ifndef GFPOW_H
#define GFPOW_H

#include <iostream>
using namespace std ;

const int deg = 8 ;     // degree of generating polynomial for finite field extension
const int pwr = 256 ;   // 2^deg
const int pwrm1 = pwr - 1 ;


//const int short static fx[deg] = {1,0,1,0,0} ;   /* 1+x^2+x^5 */
//const int short static fx[deg]= {1,0,1,1,0,0,1,0} ;  //1+x^2+x^3+x^6+x^8 
const int short static fx[deg]= {1,0,1,1,1,0,0,0} ;// 1 + x^2 + x^3 + x^4 + x^8

//extern int short mul[pwr][pwr] ;
//extern int short inv[pwr] , lg[pwr], ex[pwr];
extern unsigned char mul[pwr][pwr] ;
extern unsigned char inv[pwr] , lg[pwr], ex[pwr];

void init() ;

/// Galois field of 2^8=256 elements
class GFpow {
private:
//	unsigned int short a;
	unsigned char a;

public:
	GFpow() : a(0) {} ;
	GFpow( int x ): a(x % pwr) {} ;
	GFpow( const GFpow & x ): a( x.a) {} ;

	GFpow operator+( const GFpow & y ) const;
	GFpow operator-( const GFpow & y ) const;
	GFpow operator+() const;
	GFpow operator-() const;

	GFpow& operator+=( const GFpow & x ) ;
	GFpow& operator-=( const GFpow & x ) ;
		
	bool operator==(int z) const{ return ( this->a == z); }
	bool operator!=(int z) const{ return ( this->a != z); }
	bool operator==(const GFpow& z) const{ return ( this->a == z.a); }
	bool operator!=(const GFpow& z) const{ return ( this->a != z.a); }

	GFpow operator*( const GFpow & y ) const;	
	GFpow operator/( const GFpow & y ) const;
	GFpow& operator*=( const GFpow & x ) ;
	GFpow& operator/=( const GFpow & x ) ;

	friend ostream& operator<< (ostream & , const GFpow & ) ;
	friend istream& operator>> (istream& in,  GFpow& v) ;

	unsigned int short log(); 
    unsigned int short num(){return a;}
} ;
//-------------- Inline functions -------------
inline GFpow GFpow::operator+( const GFpow & y ) const{
	return (this->a^y.a) ;
}
inline GFpow GFpow::operator-(const GFpow & y ) const{
	return (this->a^y.a) ;
}
inline GFpow GFpow::operator+() const       // unary plus
{                                 // removed const as return  modifies j
    return *this;
}
inline GFpow GFpow::operator-() const       // unary plus
{                                 // removed const as return  modifies j
    return *this;
}

inline GFpow& GFpow:: operator+=( const GFpow & x ) {
	a= a^x.a ;
	return *this ;
}
inline GFpow& GFpow:: operator-=( const GFpow & x ) {
	a= a^x.a ;
	return *this ;
}

inline GFpow GFpow::operator*( const GFpow & y ) const{
	int j  ; 
	if (( this->a == 0 ) || ( y.a == 0 )) return (0) ;
	if ((j = lg[this->a] + lg[y.a] ) >= pwr ) j -= pwrm1 ;
	return( ex[j] ) ;
}
inline GFpow GFpow::operator/( const GFpow & y ) const{
	int j ; 
	/*if ( y.a==0 ) {
		cout << "Division by zero"<< endl ;
		abort() ;
	}*/
	if ( this->a == 0 ) return (0 ) ;
	if( (j = lg[this->a]-lg[y.a]) < 0 ) j += pwrm1 ;
	return ( ex[j] ) ;
}
inline GFpow& GFpow:: operator*=( const GFpow & x ) {
	int j  ; 
	if (this->a == 0 ) return *this;
	if ( x.a == 0 ) {this->a = 0 ; return *this ;}

	if ((j = lg[this->a] + lg[x.a] ) >= pwr ) j -= pwrm1 ;
	a = ex[j]  ;
	return *this ;
}
inline GFpow& GFpow:: operator/=( const GFpow & x ) {
	int j  ; 
	/*if ( x.a==0 ) {
		cout << "Division by zero in operator /="<< endl ;
		abort() ;
	}*/
	if ( a == 0 ) return *this ;
	if( (j = lg[a]-lg[x.a]) < 0 ) j += pwrm1 ;
	a = ex[j]  ;
	return *this ;
}

#endif  /* GFPOW_H  */
