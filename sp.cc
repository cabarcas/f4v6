/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file sp.cpp
    Implementation of the sp class functions.
*/
#include "sp.h"

//--------------------------------------------------
//--------------Constructors------------------------
//--------------------------------------------------
Sp::Sp()
{
    this->m = 0;
    this->n = 0;
    this->nzmax = 0;
    this->nz = 0;
	this->maxdel = 0;
	this->ndel = 0;
	this->val = NULL;
	this->irowst = NULL;
	this->j = NULL;
	this->linkrw = NULL;
	this->jcolst = NULL;
	this->i = NULL;
	this->linkcl = NULL;
	this->del = NULL;
}
Sp::Sp(int m1, int n1, int nzmax1)
{
    this->m = m1 ;				    // define dimensions and nzmax
    this->n = n1 ;
    this->nzmax = MAX (nzmax1, 1) ;
    this->nz = 0;
	this->maxdel = this->m;  //we predict a minimun of m deletions in gauss
	this->ndel = 0;
	//allocate memory for the values and the structure
	this->val = (Field *)calloc(this->nzmax, sizeof(Field));  //numerical value of entry i (size nzmax)
	this->irowst = (int *)calloc(this->m, sizeof(int));  //location of first entry for row i (size m)
	for(int k = 0; k < this->m; k++)
        this->irowst[k] = -1;
	this->j = (int *)calloc(this->nzmax, sizeof(int));  //column number of entry i (size nzmax)
	this->linkrw = (int *) calloc(this->nzmax, sizeof(int));//link to the next entry in the row of entry i, or -1 if last (size nzmax)
	this->jcolst = (int *) calloc(this->n, sizeof(int));//location of first entry for col i(size n)
	for(int k = 0; k < this->n; k++)
        this->jcolst[k] = -1;
	this->i = (int *) calloc(this->nzmax, sizeof(int));  //row number of entry i (size nzmax)
	this->linkcl = (int *) calloc(this->nzmax, sizeof(int)); //link to the next entry in the col of entry i, or -1 if last (size nzmax)
	this->del = (int *) calloc(this->maxdel, sizeof(int)); //link to the next entry in the col of entry i, or -1 if last (size nzmax)
}
void Sp::reset(int m1, int n1, int nzmax1)
{
    if(this->val)
    {
        free (this->val) ;
 	    free (this->irowst);
	    free (this->j);
	    free (this->linkrw);
	    free (this->jcolst);
	    free (this->i);
	    free (this->linkcl);
	    free (this->del);
    }
    this->m = m1 ;				    // define dimensions and nzmax
    this->n = n1 ;
    this->nzmax = MAX (nzmax1, 1) ;
    this->nz = 0;
	this->maxdel = this->m;  //we predict a minimun of m deletions in gauss
	this->ndel = 0;
	//allocate memory for the values and the structure
	this->val = (Field *)calloc(this->nzmax, sizeof(Field));  //numerical value of entry i (size nzmax)
	this->irowst = (int *)calloc(this->m, sizeof(int));  //location of first entry for row i (size m)
	for(int k = 0; k < this->m; k++)
        this->irowst[k] = -1;
	this->j = (int *)calloc(this->nzmax, sizeof(int));  //column number of entry i (size nzmax)
	this->linkrw = (int *) calloc(this->nzmax, sizeof(int));//link to the next entry in the row of entry i, or -1 if last (size nzmax)
	this->jcolst = (int *) calloc(this->n, sizeof(int));//location of first entry for col i(size n)
	for(int k = 0; k < this->n; k++)
        this->jcolst[k] = -1;
	this->i = (int *) calloc(this->nzmax, sizeof(int));  //row number of entry i (size nzmax)
	this->linkcl = (int *) calloc(this->nzmax, sizeof(int)); //link to the next entry in the col of entry i, or -1 if last (size nzmax)
	this->del = (int *) calloc(this->maxdel, sizeof(int)); //link to the next entry in the col of entry i, or -1 if last (size nzmax)
}
void Sp::reset_skim(int m1, int n1, int nzmax1)
{
    if(this->val)
    {
        free (this->val) ;
 	    free (this->irowst);
	    free (this->j);
	    free (this->linkrw);
	    free (this->jcolst);
	    free (this->i);
	    free (this->linkcl);
	    free (this->del);
    }
    this->m = m1 ;				    // define dimensions and nzmax
    this->n = n1 ;
    this->nzmax = MAX (nzmax1, 1) ;
    this->nz = 0;
	this->maxdel = 10;  //we predict a minimun of m deletions in gauss
	this->ndel = 0;
	//allocate memory for the values and the structure
	this->val = (Field *)calloc(this->nzmax, sizeof(Field));  //numerical value of entry i (size nzmax)
	this->irowst = (int *)calloc(this->m, sizeof(int));  //location of first entry for row i (size m)
	for(int k = 0; k < this->m; k++)
        this->irowst[k] = -1;
	this->j = (int *)calloc(this->nzmax, sizeof(int));  //column number of entry i (size nzmax)
	this->linkrw = (int *) calloc(this->nzmax, sizeof(int));//link to the next entry in the row of entry i, or -1 if last (size nzmax)

    this->jcolst = NULL;
	this->i = NULL;
	this->linkcl = NULL;
	this->del = (int *) calloc(this->maxdel, sizeof(int)); //link to the next entry in the col of entry i, or -1 if last (size nzmax)
}
Sp::Sp(const Sp &A)
{
    this->val = NULL;
    this->reset(A.m, A.n, A.nz);

	this->nz = A.nz;

	memcpy(this->val, A.val, A.nz * sizeof(Field));
	memcpy(this->irowst, A.irowst, A.m * sizeof(int));
	memcpy(this->j, A.j, A.nz * sizeof(int));
	memcpy(this->linkrw, A.linkrw, A.nz * sizeof(int));
	memcpy(this->jcolst, A.jcolst, A.n * sizeof(int));
	memcpy(this->i, A.i, A.nz * sizeof(int));
	memcpy(this->linkcl, A.linkcl, A.nz * sizeof(int));

	this->del = (int *) calloc(A.ndel, sizeof(int));
	memcpy(this->del, A.del, A.ndel * sizeof(int));
	this->maxdel = this->ndel = A.ndel;	
}
const Sp &Sp::operator=(const Sp &A)
{
    if((double)A.ndel / A.nz < 0.1)
    {
        this->reset_skim(A.m, A.n, A.nz);

	    this->nz = A.nz;

	    memcpy(this->val, A.val, A.nz * sizeof(Field));
	    memcpy(this->irowst, A.irowst, A.m * sizeof(int));
	    memcpy(this->j, A.j, A.nz * sizeof(int));
	    memcpy(this->linkrw, A.linkrw, A.nz * sizeof(int));
//	    memcpy(this->jcolst, A.jcolst, A.n * sizeof(int));
//	    memcpy(this->i, A.i, A.nz * sizeof(int));
//	    memcpy(this->linkcl, A.linkcl, A.nz * sizeof(int));

	    this->del = (int *) calloc(A.ndel, sizeof(int));
	    memcpy(this->del, A.del, A.ndel * sizeof(int));
	    this->maxdel = this->ndel = A.ndel;	
    }
    else
    {
        this->reset_skim(A.m, A.n, A.nz-A.ndel);

        this->nz = A.nz-A.ndel;

        //prepare link map
        int *link_map = (int *)calloc(A.nz, sizeof(int));
        for(int k = 0; k < A.nz; k++)
            link_map[k] = 0;
        for(int k = 0; k < A.ndel; k++)
            link_map[A.del[k]] = -1;
        int count = 0;
        for(int k = 0; k < A.nz; k++)
            if(link_map[k] != -1)
                link_map[k] = count++;
        //copy-transform elements in val, j, i, linkrw, linkcl
        for(int k = 0; k < A.nz; k++)
            if(link_map[k] != -1)
            {
                this->val[link_map[k]] = A.val[k];
//                this->i[link_map[k]] = A.i[k];
                this->j[link_map[k]] = A.j[k];
                this->linkrw[link_map[k]] = A.linkrw[k]==-1 ? -1 : link_map[A.linkrw[k]];
//                this->linkcl[link_map[k]] = A.linkcl[k]==-1 ? -1 : link_map[A.linkcl[k]];
            }
        //copy transform irowst
        for(int i = 0; i < this->m; i++)
            this->irowst[i] = A.irowst[i] == -1 ? -1 : link_map[A.irowst[i]];
        //copy transform jcolst
//        for(int j = 0; j < this->n; j++)
//            this->jcolst[j] = A.jcolst[j] == -1 ? -1 : link_map[A.jcolst[j]];
        //free link map
        free(link_map);
    }
    return *this;
}

Sp::~Sp()
{
    free (this->val) ;
    free (this->irowst);
    free (this->j);
    free (this->linkrw);
    free (this->jcolst);
    free (this->i);
    free (this->linkcl);
    free (this->del);
}
void Sp::clear()
{
    free (this->val) ;
    free (this->irowst);
    free (this->j);
    free (this->linkrw);
    free (this->jcolst);
    free (this->i);
    free (this->linkcl);
    free (this->del);
    this->val = NULL;
    this->irowst= NULL;
    this->j= NULL;
    this->linkrw= NULL;
    this->jcolst= NULL;
    this->i= NULL;
    this->linkcl= NULL;
    this->del= NULL;
}
void Sp::stretch(int nzmax1)
// change the max # of entries sparse matrix
{
    this->nzmax = nzmax1;
    this->val = (Field*) realloc (this->val, this->nzmax * sizeof(Field)) ;
    this->i = (int*) realloc (this->i, this->nzmax * sizeof(int)) ;
    this->j = (int*) realloc (this->j, this->nzmax * sizeof(int)) ;
    this->linkrw = (int*) realloc (this->linkrw, this->nzmax * sizeof(int)) ;
    this->linkcl = (int*) realloc (this->linkcl, this->nzmax * sizeof(int)) ;	
}
//--------------------------------------------------
//--------------Functions---------------------------
//--------------------------------------------------
/// add an entry to a triplet matrix; return 1 if ok, 0 otherwise 
void Sp::entry(int i, int j, Field x)
{
	//copy inputs
	int new_k = ((this->ndel > 0) ? this->del[--this->ndel] : this->nz++ );
    if (new_k >= this->nzmax)
        this->stretch(2*this->nzmax);
    this->val [new_k] = x ;
    this->i [new_k] = i ;
    this->j [new_k] = j ;
	//fix structure
	//row link (no order required)
	//insert at the begining
	this->linkrw[new_k] = this->irowst[i];  //link the new one to the first one
	this->irowst[i] = new_k;  //set the start of row to the new entry
	//col link (no order required)
	//insert at the begining
	this->linkcl[new_k] = this->jcolst[j];  //link the new one to the first one
	this->jcolst[j] = new_k;  //set the start of row to the new entry
}
/// Removes the entry in the k-th position
void Sp::remove_entry(int k, int pre_k_col)
{
	if(this->ndel >= this->maxdel)//check enough memory
	{
		this->del = (int *) realloc(this->del, 2*this->maxdel*sizeof(int));
		this->maxdel *= 2;
	}
	this->del[this->ndel++] = k;
	//fix structure
	int i = this->i[k], j = this->j[k];
	//row link
	//find the right pos to del
	int k1, pre_k;
	for(k1 = this->irowst[i], pre_k = -1; k1 >= 0 && k1 != k; k1 = this->linkrw[k1])
		pre_k = k1;
	if(pre_k < 0)//deleting the first elt
		this->irowst[i] = this->linkrw[k];
	else
		this->linkrw[pre_k] = this->linkrw[k];
	//col link (when deleting, link list must be mantained)
    if(pre_k_col != -2) //pre_k calculated in advance
        pre_k = pre_k_col;
    else
    {//find the right pos to del
	    for(k1 = this->jcolst[j], pre_k = -1; k1 >= 0 && k1 != k; k1 = this->linkcl[k1])
		    pre_k = k1;
    }
	if(pre_k < 0)//deleting the first elt
		this->jcolst[j] = this->linkcl[k];
	else
		this->linkcl[pre_k] = this->linkcl[k];
}
/// deletes row i
void Sp::remove_row(int i_rem)
{
    int k;
    //remove entries in matrix
    for(k = this->irowst[i_rem]; k >= 0; k = this->linkrw[k])
        this->remove_entry(k);
    //remove row from irowst
    for(int i = i_rem+1; i < this->m; i++)
        this->irowst[i-1] = this->irowst[i];
    this->m--;
}
/// removes all zero rows and zero entries in front of first non-zero of each row. Returns the number of rows left
int Sp::purge()
{
    int i = 0;
    bool zero_row;
	while(i < this->m)
	{
        zero_row = true;
		for(int k = this->irowst[i]; k >= 0; k = this->linkrw[k])
        {
			if(this->val[k] != 0)
            {
                zero_row = false;
                break;
            }
        }
        if(zero_row)
            this->remove_row(i);
        else
            i++;
	}
    return i;
}
void Sp::reserve_rows(int m1)
{
	while(this->m < m1)
	{
		this->irowst = (int *)realloc(this->irowst, 2*this->m*sizeof(int));
		for(int k = this->m; k < 2*this->m; k++)
            this->irowst[k] = -1;
		this->m += this->m;
	}
}
//--------------------------------------------------
//--------------Predicates--------------------------
//--------------------------------------------------
bool Sp::is_zero_col(int j) const
{
    for(int k = this->jcolst[j]; k >= 0; k = this->linkcl[k])
    {
        if(this->val[k] != 0)
            return false;
    }
    return true;
}
/// Returns the column of the first non_zero entry of row i
int Sp::first_non_zero(int i) const
{
    //search for first non-zero entry        
    int min_j = this->n + 1;
    for(int k = this->irowst[i]; k >= 0; k = this->linkrw[k])
    {
        if(this->val[k] != 0)
            min_j = MIN(min_j, this->j[k]);
    }
    return min_j;
}
/// Returns the number of non-zero rows
int Sp::nnz_rows() const
{
    int count = 0;
    bool zero_row;
	for(int i = 0; i < this->m; i++)
	{
        zero_row = true;
		for(int k = this->irowst[i]; k >= 0; k = this->linkrw[k])
        {
			if(this->val[k] != 0)
            {
                zero_row = false;
                break;
            }
        }
        if(zero_row)
            count++;
	}
    return this->m - count;
}
/// Returns the number of non-zero columns
int Sp::nnz_cols() const
{
    int count = 0;
    bool zero_col;
	for(int j = 0; j < this->n; j++)
	{
        zero_col = true;
		for(int k = this->jcolst[j]; k >= 0; k = this->linkcl[k])
        {
			if(this->val[k] != 0)
            {
                zero_col = false;
                break;
            }
        }
        if(zero_col)
            count++;
	}
    return this->n - count;
}
/// Returns the number of non-zero entries
int Sp::nnz() const
{
    return this->nz - this->ndel;
}
//--------------------------------------------------
//--------------Other functions---------------------
//--------------------------------------------------
/// copies row i_A of A into row i_B of B transforming cols of A into cols of B using fnc
int full_size_row(const Sp &A, int i, Field *aux)
{
    int max_j = -1;
    for(int k = A.irowst[i]; k >= 0; k = A.linkrw[k])
    {
        int j = A.j[k];
        aux[j] = A.val[k];
        max_j = MAX(max_j, j);
    }
    return max_j;
}
