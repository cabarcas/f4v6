/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file f4v6.cpp
    Definition of functions of the f4 algorithm.    
*/
#include<algorithm>
#include "f4v6.h"
#include "output.h"

/// Finds a Groebner basis for F and stores it on G using the F4 algorithm
/**-----------PSeudocode------------(adapted from Faug�re99)
Input: F
Output: G, a Groebner basis for <F>
G = \EMPTY
B = \EMPTY
HH[0] = \empty
GG[0] = F
j = 1
for all f \in F 
	(G,B) = update(G,B,f);
while B != \empty
	B*={b\in B : deg(b) is minimal }
	B = B \ B*
    L := {(t,f) : {f,g}\in B*, t = lcm(LM(f),LM(g))/LM(f)}
    H = HH[j] = symbolic_preprocessing(L, G, (HH[0],...HH[j-1]), (GG[0],...GG[j-1]) )
	Htilde = GG[j] = ReductionRowEchelon(H)
	Htilde+ = {h \in Htilde : LM(h) \notin LM(H)}
	for each h \in Htilde+
		(G,B) = update(G,B,h)
    j = j+1;
*/
void F4(const List_poly &F,  //< input sequence
        List_poly &G	     //< output GB of <F>. 
        )
{
    //Start F4
    vector<list<Signature> > HH;                 //< Stores signatures of H after symbolic preprocessing on each step
    vector<List_poly> GG;                       //< Stores Htilde after row reduction on each step
	vector<list<Poly_poly>* > BB(MAX_DEG, (list<Poly_poly>*)NULL); //< Stores all unprocessed pairs discriminated by degree
    vector<vector<bool> > ncsry;                 //< ncsry[step][i]==true iff the i-th poly in GG[step] is necessary for making pairs
    //reserve space for HH GG and unnecessary
    HH.reserve(MAX_STEP);
    GG.reserve(MAX_STEP);
    ncsry.reserve(MAX_STEP);
    //step zero
    HH.push_back(list<Signature>());            //< HH[0] = \empty
    GG.push_back(F);                            //< GG[0] = F
    ncsry.push_back(vector<bool>(F.size(),true)); //< create necessary vector for new elts F. All true
    update(GG, &BB, &ncsry);
    //prepare for step 1
	short deg = select(BB);            //< degree of the step
    Monomial::increase_indexed(deg);   //< indexes monomials up to degree deg
    while( deg != -1 && GG.size() <= MAX_STEP)  //< while there are still pairs in BB and not too many steps
	{
        list<Signature> L;      //< A list of monomial-polynomial pairs
		left_right(*(BB[deg]), GG, &L);
        delete BB[deg]; BB[deg] = NULL; //<delete the pairs used
        List_poly H(deg);       //< A list of polynomials of degree up to deg
        symbolic_preprocessing(GG, ncsry, HH, &H, &L);
        HH.push_back(L);    //< signatures of H
        L.clear();
        //make a copy of the leading monomials of H before gauss
        set<Monomial::Index> lead_monomial_H(H.lead_monomial_begin(), H.lead_monomial_end());
		reduction_row_echelon(&H);
        GG.push_back(H);    //< store permanently H in GG.
        H.clear();
        ncsry.push_back(vector<bool>(GG.back().size(),true)); //< create necessary vector for new elts H
        //mark as unnecessary if leading monomial appears in the leading monomials of H before gauss
        remove_if_LM_in(GG.back(), &(ncsry.back()), lead_monomial_H);
        lead_monomial_H.clear();
        //prepare for next step
        update(GG, &BB, &ncsry);
        deg = select(BB);
        Monomial::increase_indexed(deg);   //< indexes monomials up to degree deg
	}
    //Prepare Output. Move necessary to G
    deg = 0;
    for(unsigned short s = 0; s < GG.size(); s++)
        deg = MAX( deg, GG[s].degree() );
    G.reset(deg);
    for(unsigned short s = 0; s < GG.size(); s++)
    {
        for(int i_f = 0; i_f < GG[s].size(); i_f++)
        {
            if(ncsry[s][i_f])
            {
                int i_g = G.new_poly();
                multiply(0, GG[s], i_f, &G, i_g);
            }
        }
    }
}
///implements the normal selection strategy for F4
/** returns the minimum degree of any pair in B
    the degree of a pair(f,g) is the degree of lcm(LM(f),LM(g)).
    \param B a list of pairs to select form
    \return the minimum degree of elements from B
*/
short select(vector<list<Poly_poly>*> &BB)
{
    short i;
    for(i = 0; i < MAX_DEG; i++)
    {
        if(BB[i])
        {
            if(BB[i]->size() == 0)
            {
                delete BB[i];
                BB[i] = NULL;
            }
            else
                return i;
        }
    }
    return -1;
 }
/// Stores in L the left and right monomial-polys products of the pairs in Bstar 
/// and their corresponding multiplication-simplification in H
/** 
    L = { (t,f) : {f,g}\in B^*, t=lcm(LM(f),LM(g))/LM(f) }
*/
void left_right(const list<Poly_poly> &Bstar,   ///< Input, selected pairs
                const vector<List_poly> &GG,    ///< Input, all computed polynomials
                list<Signature> *L              ///< Output, Signatures (t,f) from B* elements
                )
{
    Monomial::Index lt1, lt2;           //< leading monomials of the polys corresponding to i_f1 and i_f2
    list<Poly_poly>::const_iterator i_B;//< an iterator on Bstar
    Signature sig1, sig2;               //< signatures to be stored in L
    for(i_B = Bstar.begin(); i_B != Bstar.end(); i_B++) //< for each pair
    {
        //basic info of the signatures
        sig1.step = i_B->step1;
        sig2.step = i_B->step2;
        sig1.i_f = i_B->i_f1;
        sig2.i_f = i_B->i_f2;
        lt1 = GG[sig1.step].lead_monomial(sig1.i_f);
        lt2 = GG[sig2.step].lead_monomial(sig2.i_f);
        sig1.lt = sig2.lt = lcm_index(lt1, lt2);
        sig1.t = quo_index( sig1.lt , lt1 );
        sig2.t = quo_index( sig2.lt , lt2 );
        //insert if nor redundant
        if( find(L->begin(), L->end(), sig1) == L->end() )
            L->push_back(sig1);
        if( find(L->begin(), L->end(), sig2) == L->end() )
            L->push_back(sig2);
    }
}
/// Enlarges H with polys of the form t*f, t a monomial, f a poly in GG s.t LM(t*f) is a monomial in H.
/** Sudocode adapted from Faugere 99
    H = { mult(simplify(t,f,HH,GG)): (t,f)\in L }
    done = LM(H)
    WHILE( M(H)\neq done )
    {
	    let t be an element of M(H)\done
	    done = done \UNION {t}
	    IF(there exist g\in G s.t. LM(g)|t)
        {
		    choose g\in G s.t. LM(g)|t
		    H = H\UNION { mult(simplify(t/LM(g),g,HH,GG)) }
	    }
    }
RETURN(H)
*/
void symbolic_preprocessing(const vector<List_poly> &GG,        ///< Input, all computed polynomials
                            const vector<vector<bool> > &ncsry,  ///< Input, all necessary elements of GG
                            const vector<list<Signature> > &HH,  ///< Input, previous signatures
                            List_poly *H,       ///< Output, new polynomials
                            list<Signature> *L  ///< Input/Output, signatures of elements of H
                            )
{
    //H = { mult(simplify(t,f,HH,GG)): (t,f)\in L }
    for(list<Signature>::iterator isig = L->begin(); isig != L->end(); isig++)
    {
        simplify(GG, HH, &(*isig));
        int i_h = H->new_poly();   //< Adds a new zero polynomial to H and returns its corresponding index            
        multiply(isig->t, GG[isig->step], isig->i_f, H, i_h);
    }
    Signature sig;  //< stores a signature to search for. 
    // leading monomials of H in a set that allows for log(n) time search
    set<Monomial::Index> H_lead_monomial(H->lead_monomial_begin(), H->lead_monomial_end());
    //for each monomial up to the largest degree of H
    for(sig.lt = Monomial::max_monomial(H->degree()); sig.lt >= 0; sig.lt--)
    {
        //lt \in M(H) and lt \notin LM(H)
        if( ! H->all_zero_coefs(sig.lt) &&                                               
            find(H_lead_monomial.begin(), H_lead_monomial.end(), sig.lt) == H_lead_monomial.end() )
        {
            //search for a LM that divides t on previous steps  
            bool found = false;
            for(sig.step = GG.size()-1; sig.step >= 0; sig.step--)//< for each step
            {
                const List_poly &G = GG[sig.step];   //< List of polys corresponding to that step
                const vector<bool> &ncsry_step = ncsry[sig.step];   //< corresponding necessary elts
                int m = G.size();   //< Number of polys in G
                for(sig.i_f = m-1; sig.i_f >= 0; sig.i_f--)     //< for each poly in G
                {
                    if(ncsry_step[sig.i_f]) //< if it is necessary
                    {
                        if(does_divide(G.lead_monomial(sig.i_f), sig.lt))    //< if LM(f) divides t
                        {
                            sig.t = quo_index(sig.lt, G.lead_monomial(sig.i_f));  //<compute the quotient t / LM(f)
                            simplify(GG, HH, &sig);
                            int i_h = H->new_poly();    //< index of a new zero polynomial in H
                            multiply(sig.t, GG[sig.step], sig.i_f, H, i_h);
                            L->push_back(sig);  //< insert corresponding signature in L
                            found = true; //< to break the for(sig.step loop
                            break;  //< breaks for(sig.i_f loop
                        }
                    }
                }
                if(found)
                    break;  //< breaks for(sig.step loop
            }
        }
    }
}

/// Simplifies the monomial-poly product (t,GG[step][i]) based on HH and GG.
/** ----- Faugere's recursive seudocode. 
    for each divisor u of t
	    if there exist H in HH s.t. uf in H
		    let p in Htilde be s.t. LM(p)=LM(uf)
		    if u == t
			    return (1,p)
		    else
			    return simplify(t/u,p)
    return (t,f)

    ---- Cabarcas' iterative seudocode
    flag = true;
    while flag
    {
        flag = false;
        for each divisor u of t
	        if there exist H in HH s.t. uf in H
		        let p in Htilde be s.t. LM(p)=LM(uf)
		        if u == t                    
			        return (1,p)
		        else
			        t = t/u;
                    f = p;
                    flag = true;
                    break for loop;
    }
    return (t,f)        
*/
void simplify(const vector<List_poly> &GG,          ///< Input, all computed polynomials
              const vector<list<Signature> > &HH,    ///< Input, previous signatures
              Signature *sig    ///< Input/output, signature to be simplified
              )
{
    Monomial t( *(to_monomial(sig->t)) );    //< Monomial corresponding to sig.t
    Monomial uu;                            //< divisor of t
    short dd = -1;//< degree of divisor. Initially unknown. Serves as flag. -1 tells the program that no u is known yet
    short d_t = degree(t);  //< degree of t
    //algorithm starts
    while (true)
    {        
        //Next divisor
        if(dd == -1)    //< if new t in place
            first_divisor(t, &uu, &dd);
        else if( !next_divisor(t, &uu, &dd) )
                break; //break while(true) loop
        //initialize d_f
        short d_f = degree(GG[sig->step].lead_monomial(sig->i_f)); //< degree of f
        //search for (u,f) in HH
        bool uf_found = false;  //< A flag that tells if uf has been found
        short uf_step;    //< index of step where looking for uf
        list<Signature>::const_iterator iter;   //< iterator on the signatures of HH
        for(uf_step = 0; uf_step < (short)HH.size(); uf_step++)
        {
            const list<Signature> &H = HH[uf_step];     //< A list of polynomials from HH.
            //search for (u,f in H)
            iter = H.begin();
            if(iter != H.end() && degree(iter->lt) == dd+d_f)
            {
                while(iter != H.end())
                {
                    if(uu == iter->t && sig->step == iter->step && sig->i_f == iter->i_f)
                    {
                        uf_found = true;                        
                        break;          //< breaks the while loop
                    }
                    iter++;
                }
                if(uf_found)
                    break;              //< breaks the i for loop
            }
        }
        if(uf_found)    //< if there exist s in HH s.t. uf in H
        {
            sig->step = uf_step;
            //search for p in GG[sig.step] s.t. LM(p)=LM(uf)
            int i_p = find_LM(GG[sig->step], iter->lt);    //< index of p in GG[sig.step]
            if (uu == t)
            {
	            //return (1,p)
                sig->t = 0;
                sig->i_f = i_p;
                return;
            }
            else
            {
                //return (t/uu, p)
                t = quo(t,uu);
                d_t = degree(t);
                sig->i_f = i_p;
                dd = -1;         //serves as flag that tells the program that u is not initialized
            }
        }//< if there exist H in HH s.t. uf in H
    }//while true
    sig->t = t.to_index();
}
/// Removes g from G if LM(g) \in monomials by marking g as unnecessary in ncsry
/** The result is that ncsry[i]==true iff LM(g) \notin monomials
*/
void remove_if_LM_in(const List_poly &G,    ///< Input, a list of polynomials
                     vector<bool> *ncsry,   ///< Output, a boolean vector of the same size as G
                     const set<Monomial::Index> &monomials  ///< Input, a set of monomials
                     )
{
    for(int i = 0; i < G.size(); i++)
        if( monomials.find(G.lead_monomial(i)) != monomials.end() )
            (*ncsry)[i] = false;
}
///Updates the pairs in BB and ncsry based on the latest inserted polynomials in GG
/*-----------PSeudocode------------(adapted from Becker)
last = GG.size
G = \union(GG[i] : i = 0,...,last-1)
Htilde+ = GG[last]
for h \in Htilde+
{
    C = {{h,g}:g \in G}
    for each {h,g1} \in C
    {
	    if (LM(h) and LM(g1) are NOT disjoint) AND
		    (there exist {h,g2} \in C\{{h,g1}} s.t. lcm(LM(h),LM(g2)) | lcm(LM(h),LM(g1)) )
	    {
		    C = C\{h,g1}
	    }
    }
    for each {h,g} \in C
    {
	    if (LM(h) and LM(g) are disjoint) 
		    C = C\{h,g}
    }
    for each {g1,g2}\in BB
    {
	    if( LM(h) | lcm(LM(g1),LM(g2)) AND
		    lcm(LM(g1),LM(h)) != lcm(LM(g1),LM(g2)) AND
		    lcm(LM(h),LM(g2)) != lcm(LM(g1),LM(g2))  )
	    {
		    BB = BB - {g1,g2}
	    }
    }

    BB = BB \UNION C

    for each g \in G \UNION H[0,...,*h-1]
	    if( LM(h) | LM(g) )
		    ncsry(g) = false
}
*/
void update(const vector<List_poly> &GG,    ///< Input/output, all computed polynomials
            vector<list<Poly_poly>*> *BB,   ///< Input/output, all pairs left to be processed
            vector<vector<bool> > *ncsry     ///< Input/output, all necessary elements of GG
            )
{
    const List_poly &Htilde = GG.back();    //< A reference to the last list in GG. Htilda+ in Faugere
    for(int i_h = 0; i_h < Htilde.size(); i_h++)    //< For each h in Htilde
    {
        if((ncsry->back())[i_h])    //< if i_h is necessary
        {
            int n_polys;        //< number of polynomials in GG[step]
            Monomial::Index lt_h = Htilde.lead_monomial(i_h); //< leading monomial of h
            vector<short> GG_step_size(GG.size());  //< size of GG[step]
            Monomial **lcm_h_g = new Monomial*[GG.size()]; //< The lcm between h and g for each g in G
            bool** disj = new bool*[GG.size()]; //< true if LM(h) and LM(g) are disjoint for each g in G
            bool** pair_of_C = new bool*[GG.size()]; //< true if h,g has not been deleted
            //-------------initialize lcm_h_g and disj and pair_of_C------------------------------------
            unsigned short step;
            for(step = 0; step < GG.size(); step++)
            {
                GG_step_size[step] =
                n_polys = step < GG.size() - 1  //< if not the last step
                    ? GG[step].size()           //< simply the size of GG[step]
                    : i_h; //                   //< else, i_h
                // set size of lcm_h_g and disj and pair_of_C to n_polys
                lcm_h_g[step] = new Monomial[n_polys];
                disj[step] = new bool[n_polys];
                pair_of_C[step] = new bool[n_polys];
                // Initialize all pair_of_C to true. This virtually forms the set C.
                for(int i = 0; i < n_polys; i++)
                    pair_of_C[step][i] = true;
                for(int i_g1 = 0; i_g1 < n_polys; i_g1++)
                {
                    if( (*ncsry)[step][i_g1] )  //< if g1 is necessary. Do not consider pair otherwise
                    {
                        Monomial::Index lt_g1 = GG[step].lead_monomial(i_g1);
                        lcm_h_g[step][i_g1] = lcm(lt_h, lt_g1);
                        disj[step][i_g1] = are_disjoint(lt_h, lt_g1);
                    }
                }
            }
            //------------Loop 1: eliminate not disjoint but lcm divisible but other lcm from new pairs----------------
            for(step = 0; step < GG.size(); step++)
            {
                n_polys = GG_step_size[step];//lcm_h_g[step].size();
                for(int i_g1 = 0; i_g1 < n_polys; i_g1++)   //< for each g1 \in G
                {   
                    if( (*ncsry)[step][i_g1] )   //< if g1 is necessary. Do not consider pair otherwise
                    {
                        if( !disj[step][i_g1] )  //< AND (LM(h) and LM(g1) are NOT disjoint)
                        {
                            unsigned int step_g2;
                            bool pair_del = false;  //< A flag to break the for loop when a pair is deleted
                            for(step_g2 = 0; !pair_del && step_g2 < GG.size(); step_g2++)
                            {
                                int i_g2;
                                int n_polys_g2 = GG_step_size[step_g2];//lcm_h_g[step_g2].size();
                                for(i_g2 = 0; !pair_del && i_g2 < n_polys_g2; i_g2++) //< for each g2 in G
                                {
                                    if( (*ncsry)[step_g2][i_g2]             //< if g2 is necessary. Otherwise {h,g2} \notin C
                                        && pair_of_C[step_g2][i_g2]        //< AND {h,g2} has not been deleted
                                        && (step_g2 != step || i_g1 != i_g2) //< AND g1 != g2
                                        && does_divide(lcm_h_g[step_g2][i_g2],  lcm_h_g[step][i_g1]) ) //< AND lcm(LM(h),LM(g2)) | lcm(LM(h),LM(g1)) 
                                    {	       
                                        pair_of_C[step][i_g1] = false;   //< C = C\{h,g1}
                                        pair_del = true;    //< break
                                    }
                                }//for i_g2
                            }//for step_g2
                        }//if !disj
                    }
                }//for i_g1
            }//for step
            //------------Loop 2: eliminate disjoint from new pairs----------------------------------------------------
            for(step = 0; step < GG.size(); step++)
            {
                n_polys = GG_step_size[step];//lcm_h_g[step].size();
                for(int i_g1 = 0; i_g1 < n_polys; i_g1++)   //< for each g1 \in G
                {
                    //if necessary
                    if( (*ncsry)[step][i_g1]        //< if g1 is necessary
                        && pair_of_C[step][i_g1]   //< AND {h,g1} has not been deleted
                        && disj[step][i_g1] )       //< AND (LM(h) and LM(g1) are disjoint)
                    {
                        pair_of_C[step][i_g1] = false;   //< C = C\{h,g1}
                    }
                }//for i_g1
            }//for step
            //------------Loop 3: eliminate from old pairs those whose lcm is divisible by LM(h) and...------------------
            for(short d = 0; d < (short)BB->size(); d++)  //< for each degree
            {
                list<Poly_poly>::iterator p;  //< iterator of a pair in BB[d]
                if((*BB)[d])    //< if there are pairs of degree d
                {
                    for(p = (*BB)[d]->begin(); p != (*BB)[d]->end(); ) //< for each pair of degree d
                    {
                        Monomial::Index lt_g1 = GG[p->step1].lead_monomial(p->i_f1),
                            lt_g2 = GG[p->step2].lead_monomial(p->i_f2);
                        Monomial lcm_g1_g2 = lcm( lt_g1, lt_g2 );   //compute lcm(LM(g1),LM(g2))
                        if(does_divide(Htilde.lead_monomial(i_h), lcm_g1_g2)        //< if LM(h) | lcm(LM(g1),LM(g2))
                            && (// !(*ncsry)[p->step1][p->i_f1]                   //< AND ( g1 is not necessary
                                // || 
                                     lcm(lt_h, lt_g1) != lcm_g1_g2 )      //< OR lcm(LM(g1),LM(h)) != lcm(LM(g1),LM(g2)) )
                            && (// !(*ncsry)[p->step2][p->i_f2]                   //< AND ( g2 is not necessary 
                                // || 
                                     lcm(lt_g2, lt_h) != lcm_g1_g2 )      //< OR lcm(LM(h),LM(g2)) != lcm(LM(g1),LM(g2)) )
                           )
                        {
                            p = (*BB)[d]->erase(p);
                        }
                        else
                            p++;
                    }//for p
                }//if BB[d]
            }//for d
            //------------Append new pairs to BB---------------------------------------------------------------------------
            for(step = 0; step < GG.size(); step++)
            {
                n_polys = GG_step_size[step];//lcm_h_g[step].size();
                for(int i_g1 = 0; i_g1 < n_polys; i_g1++)   //< for each g1 \in G
                {
                    if( (*ncsry)[step][i_g1]        //< if g1 is necessary
                        && pair_of_C[step][i_g1] )  //< AND the {h,g1} has not been erased
                    {
                        //append to BB at the appropiate degree
                        short d = degree(lcm_h_g[step][i_g1]);
                        if(d >= MAX_DEG)
                            exit(2);
                        if(!(*BB)[d])
                            (*BB)[d] = new list<Poly_poly>;
                        (*BB)[d]->push_back(Poly_poly(step, i_g1, GG.size()-1, i_h));
                    }
                }//for i_g1
            }//for step
            //------------Loop 4: mark as unnecessary the elements of G (including already processed h \in Htilde) divisible by h
            for(step = 0; step < GG.size(); step++)
            {
                n_polys = GG_step_size[step];//lcm_h_g[step].size();
                for(int i_g1 = 0; i_g1 < n_polys; i_g1++)   //< for each g \in G
                {
                    if( (*ncsry)[step][i_g1]                                //< if g1 is necessary
                        && does_divide(lt_h, GG[step].lead_monomial(i_g1)) )    //< AND LM(H) | LM(g)
                    {
                        (*ncsry)[step][i_g1] = false;   //< mark g as unnecessary for making pairs
                    }
                }//for i_g1
            }//for step
            //------------Release memory----------------------------------------------------------
            for(step = 0; step < GG.size(); step++)
            {
                delete[] lcm_h_g[step];
                delete[] pair_of_C[step];
                delete[] disj[step];
            }
            delete[] lcm_h_g;
            delete[] pair_of_C;
            delete[] disj;
        }//if i_h is necessary
    }//end for h in Htilda
}
