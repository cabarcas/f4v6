/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file list_poly.cpp
    Implementation of the List_poly class functions.
*/
#include<algorithm>
#include "list_poly.h"
#include "output.h"
//---------------------------------------------------------------------------------------
//-----------------------------------Construction & destructions--------------------------
//---------------------------------------------------------------------------------------
/// Initializes The list with maximum degree d
List_poly::List_poly(const short d)
{
    int n = Monomial::max_monomial(d)+1;    //< Number of colums = Maximum number of monomials up to degree d
    this->deg = d;
    this->coef.reset(n, n, n); //< Initializes a matrix with n rows n cols and n max non-zero entries 
    this->m_poly = 0;
    this->lead_t.reserve(n);
}
/// Copy constructor
List_poly::List_poly(const List_poly &F)
{
    this->deg = F.deg;
    this->coef = F.coef;
    this->lead_t = F.lead_t;
    this->m_poly = F.m_poly;
}
/// Clears the list and sets the maximum degree to d
void List_poly::reset(const short d)
{
    int n = Monomial::max_monomial(d)+1;    //< Number of colums = Maximum number of monomials up to degree d
    this->deg = d;
    this->coef.reset(n, n, n); //< Initializes a matrix with n rows n cols and n max non-zero entries 
    this->m_poly = 0;
    this->lead_t.clear();
    this->lead_t.reserve(n);
}
/// Erases all elements and releases memory
void List_poly::clear()
{
    this->deg = 0;
    this->coef.clear();
    this->m_poly = 0;
    this->lead_t = vector<Monomial::Index>();
}
//---------------------------------------------------------------------------------------
//-----------------------------------functions to modify the list------------------------
//---------------------------------------------------------------------------------------
/// Inserts n zero polynomials at the end and returns the position of the first inserted.
int List_poly::new_poly(const int n)
{
    int m = this->m_poly;
    this->m_poly += n;
    this->coef.reserve_rows(this->m_poly);
    if((int)(this->lead_t.capacity()) < m + n)  //< If the capacity is less than necessary
        this->lead_t.reserve(MAX(2*m, m + n) );    //< Increment capacity 
    this->lead_t.insert(this->lead_t.end(), n, -1);    //< Insert -1 as leading monomial of new polynomials
    return m;    //< Since it is a sparse implementation, zero polynomials need not be initialized
}
/// Removes i-th poly.
void List_poly::remove_poly(const int i_rem)
{
    this->coef.remove_row(i_rem);
    this->m_poly--;
    this->lead_t.erase( this->lead_t.begin() + i_rem );
}
//---------------------------------------------------------------------------------------
//-----------------------------------other functions-------------------------------------
//---------------------------------------------------------------------------------------
/// Updates the leading monomials for the elements in H
void update_lead_monomial(List_poly *H)
{
    //remove zero rows and 0 entries at the beginning of a row
    H->m_poly = H->coef.purge();
    //update LTs
    H->lead_t.clear();
    H->lead_t.reserve(H->m_poly);   //Should not be necessary but just to make sure.
    for(int i = 0; i < H->m_poly; i++)
    {        
        H->lead_t.push_back( H->col_to_monomial( H->coef.first_non_zero(i) ) );
    }
}
/// Reduces H to row echelon form as defined in Faugere 99
void reduction_row_echelon(List_poly *H)
{
//-------------timing patch----------------------
#ifdef PRINT_STATS
    int nnz_p = H->nnz_polys(),
        nnz_t = H->nnz_monomials(),
        nnz_c = H->nnz_coefs();
    for(double thr = 0.01; thr <= .1; thr += .01)
    {
        int *pivot_col = new int[H->lead_t.size()];
        for(int i = 0; i < H->m_poly; i++)
            pivot_col[i] = -1;
        Sp aux_matrix(H->coef);
        fout<<endl<<nnz_p
            <<'\t'<<nnz_t
            <<'\t'<<nnz_c
            <<'\t'<<thr;
        clock_t timer1 = clock();
        gauss(aux_matrix, pivot_col, thr);
        fout<<'\t'<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
        delete pivot_col;
    }
#endif
//-------------timing patch end----------------------

    int *pivot_col = new int[H->lead_t.size()];
    for(int i = 0; i < H->m_poly; i++)
        pivot_col[i] = -1;
    //gauss(H->coef, pivot_col, H->lead_t.size(), 0.2);
    gauss(H->coef, pivot_col, 0.1);
    int m = 0;
    for(int i = 0; i < H->m_poly; i++)
    {
        if(pivot_col[i] < 0)  //< If no pivot in this row
        {
            H->coef.remove_row(m);
        }
        else
        {
            H->lead_t[m] = H->col_to_monomial(pivot_col[i]);
            m++;
        }
    }
    H->m_poly = m;
    H->lead_t.resize(m);
    delete pivot_col;
}
///Searches for g \in G s.t LM(g) = t
/** Returns the first index in G of a polynomial with leading monomial t
    or -1 otherwise.
*/
int find_LM(const List_poly &G, const Monomial::Index t)
{
    int i_g;    //< index on G
    for(i_g = 0; i_g < G.size(); i_g++)
        if( G.lead_monomial(i_g) == t )
            return i_g;
    return -1;
}
/// A function object that transforms a col j of G into a col of H after multiplying by t
class col_transform
{
private:
    const Monomial &t;
    const List_poly &G, &H;
public:
    col_transform(const Monomial &t1, const List_poly &G1, const List_poly &H1) : t(t1), G(G1), H(H1){}
    int operator()(int j)
    {
        return H.monomial_to_col(mult_index( t, G.col_to_monomial(j)));
    }
};
/// Multiplies the poly in G[i] by the monomial t and stores the result in H[i_h]
/** Assumes that H[i_h] is a zero polynomial
*/
void multiply(const Monomial &t, const List_poly &G, int i_g, List_poly *H, const int i_h)
{
    copy_transform(G.coef, i_g, H->coef, i_h, col_transform(t, G, *H));
    H->lead_t[i_h] = mult_index(t, G.lead_t[i_g]);
}
