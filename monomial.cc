/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file monomial.cpp
    Implementation of the monomial class functions.
*/
#include "monomial.h"

/** \file monomial.cpp
    \brief Implements basic Monomial functions for the general case of K[x_1,...,x_n]
*/

/// Compares t1 and t2 in glex.
/** \return 0 if t1 == t2,
            < 0 if t1 < t2,
            > 0 if t2 < t1
*/
short compare(const Monomial &t1, const Monomial &t2)
{
    short d1 = degree(t1),
        d2 = degree(t2);
    if(d1 < d2)
        return -1;  //t1 < t2
    if(d2 < d1)
        return 1;   //t2 < t1
    //Else degrees equal. Check lex
    Monomial::const_iterator i1 = t1.begin(),
        i2 = t2.begin();
    while(i1 != t1.end() && i2 != t2.end())
    {
        if(i1->first < i2->first)
            return 1;   //t2 < t1
        if(i2->first < i1->first)
            return -1;   //t1 < t2
        //Else same variable, compare exponents
        if(i1->second < i2->second)
            return -1;   //t1 < t2
        if(i2->second < i1->second)
            return 1;   //t2 < t1
        //Else same exponent, move to next variable
        i1++;
        i2++;
    }
    return 0;
}
/// Returns the degree of the monomial t. If t is too high, returns -1
short degree(const Monomial::Index t)
{
    short d;
    for(d = 0; d < (int)Monomial::deg_sep.size(); d++)
        if(Monomial::deg_sep[d] >= t)
            return d;
    return -1;
}
/// Returns the degree of the monomial t
short degree(const Monomial &t)
{
    short s = 0;
    Monomial::const_iterator i;
    for(i = t.begin(); i != t.end(); i++)
        s += i->second;
    return s;
}
/// Returns t1*t2 as a Monomial
Monomial mult(const Monomial &t1, const Monomial &t2)
{
    Monomial r = t2;                    //<result. Initialized to t2
    Monomial::const_iterator i1;        //< iterator through the variables of t1
    Monomial::iterator i2;              //< iterator through the variables of r
    i2 = r.begin();
    for(i1 = t1.begin(); i1 != t1.end(); i1++)
    {
        while(i2 != r.end() && i1->first > i2->first)
            i2++;
        if(i2 == r.end() || i1->first != i2->first)//< if different variable
            r[i1->first] = i1->second;
        else
            i2->second += i1->second;
    }
    return r;
}
/// Returns t1*t2 as and int
Monomial::Index mult_index(const Monomial &t1, const Monomial::Index t2)
{
    Monomial::Index r = t2;                 //< result. Initialized to t2.
    Monomial::const_iterator i;             //< iterator through the variables of t1
    short d;                            //< degree of a variable of t1
    for(i = t1.begin(); i != t1.end(); i++)
    {
        for(d = 0; d < i->second; d++)
        {
            r = Monomial::mult_table[i->first + Monomial::num_var * r];    //x_i * indx_monomials[j] = indx_monomials[mult_table[i + num_var*j]]
        }
    }
    return r;
}
/// Returns t1/t2. Assumes t2 divides t1.
Monomial quo(const Monomial &t1, const Monomial &t2)
{
    Monomial q = t1;  //< To store the quotient. Initialized with t1
    Monomial::const_iterator i;             //< iterator through the variables of t2
    for(i = t2.begin(); i != t2.end(); i++) //for each variable in t2
    {
        // find the variable in q
        Monomial::iterator i_q = q.find(i->first);
        if(i_q->second > i->second)    //< if the exponent of q is larger than that of t2
            i_q->second -= i->second;
        else if(i_q->second == i->second)   //< if they are equal
            q.erase(i_q);
    }
    return q;
}
/// Returns -1 if t2 dors not divide t1, t1/t2 otherwise
Monomial::Index quo_index(const Monomial &t1, const Monomial &t2)
{
    Monomial q = t1;  //< To store the quotient. Initialized with t1
    Monomial::const_iterator i;             //< iterator through the variables of t2
    for(i = t2.begin(); i != t2.end(); i++) //for each variable in t2
    {
        // find the variable in q
        Monomial::iterator i_q = q.find(i->first);
        if(i_q == q.end())                  //< if the variable is not present in q
            return -1;
        else if(i_q->second > i->second)    //< if the exponent of q is larger than that of t2
            i_q->second -= i->second;
        else if(i_q->second == i->second)   //< if they are equal
            q.erase(i_q);
        else                                //< otherwise it doesn't divide
            return -1;
    }    
    return q.to_index();
}
/// Returns true if t1 divides t2
bool does_divide(const Monomial &t1, const Monomial &t2)
{
    Monomial::const_iterator i1;                 //< iterator through the variables of t2
    for(i1 = t1.begin(); i1 != t1.end(); i1++) //for each variable in t2
    {
        // find the variable in t1
        Monomial::const_iterator i2 = t2.find(i1->first);
        // if the variable is not present in t2 OR the exponent of t2 is less than that of t1
        if(i2 == t2.end() || i2->second < i1->second )
            return false;
    }    
    return true;
}
/// finds the least comon multiple of t1 and t2
Monomial lcm(const Monomial &t1, const Monomial &t2)
{
    Monomial lcmt = t1;  //< To store the lcm. Initialized with t1
    Monomial::const_iterator i;             //< iterator through the variables of t2
    for(i = t2.begin(); i != t2.end(); i++) //for each variable in t2
        lcmt[i->first] = MAX(lcmt[i->first], i->second);
    return lcmt;
}
/// finds the least comon multiple of t1 and t2
/** Only use if it is known that the result's deg is <= cur_deg
*/
Monomial::Index lcm_index(const Monomial &t1, const Monomial::Index t2)
{
    Monomial::Index r = t2;                 //< result. Initialized to t2.
    Monomial::const_iterator i;             //< iterator through the variables of t1
    short d;                            //< degree of a variable of t1
    for(i = t1.begin(); i != t1.end(); i++) //for each variable in t1
    {
        const Monomial &t_r = Monomial::indx_monomials[r];
        Monomial::const_iterator i_r = t_r.find(i->first);
        // Find the difference between the exponent of i and the corresponding of r. zero if negative.
        short diff = MAX(   i->second - (i_r == t_r.end() ? 0 : i_r->second),
                            0);
        for(d = 0; d < diff; d++)
            r = Monomial::mult_table[i->first + Monomial::num_var * r];    //x_i * indx_monomials[j] = indx_monomials[mult_table[i + num_var*j]]
    }
    return r;
}
/// Returns true if the monomials t1 and t2 are disjoint
bool are_disjoint(const Monomial &t1, const Monomial &t2)
{
    Monomial::const_iterator i;             //< iterator through the variables of t2
    for(i = t2.begin(); i != t2.end(); i++) //for each variable in t2
    {
        if(t1.find(i->first) != t1.end()) //< if the variable appears in t1
            return false;
    }
    return true;
}
