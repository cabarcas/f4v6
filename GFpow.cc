/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file GFpow.cpp
    Implementation of the GFpow class functions.
*/
#include "GFpow.h"

//int short mul[pwr][pwr] ;
//int short inv[pwr] , lg[pwr], ex[pwr];
unsigned char mul[pwr][pwr] ;
unsigned char inv[pwr] , lg[pwr], ex[pwr];

unsigned int short GFpow::log()
{
    return lg[a];
}

void mult( int * c, int * a, int * b ) {
	/* multiplication of polynomials mod 2                    */
	/* c polynomial of degree 14,                             */
	/* x^7+s polynomials of degree 7, with x^7 not stored,    */ 
	/* Compute  c = c mod s                                   */
	int i,j ; 
	for ( i = 0; i < 2*deg-1 ; i++ ) c[i] = 0 ;
	for ( i = 0; i < deg; i++ ) 
		if ( a[i]==1 ) for ( j = 0 ; j < deg ; j++ ) {
			c[i+j] += b[j] ;
			if ( c[i+j] == 2 ) c[i+j]=0 ;
		}
		for( j = 2*deg-2 ; j >= deg ; j-- ) {
			if ( c[j] == 1 ) {
				c[j] = 0 ;
				for ( i = 0 ; i < deg ; i++ ) {
					c[j-1-i] += fx[deg-1-i] ;
					if ( c[j-1-i] == 2 ) c[j-1-i] = 0 ;
				}
			}
		}
}

void binaryform( int * a, int n) {//Change an integer into polynomail(An coefficient array)
	int i ;
	for ( i=0 ; i < deg ; i++ ) {//get the last digit of n each time and put into a[i]
		a[i] = n & 1 ;
		n = n >> 1 ;
	}
}


int eval ( int * c ) {
	/* convert polynomial to integer between 0 to 256 */
	int i, a = 0 ;
	for ( i = deg-1; i >= 0; i-- ) 
		a = c[i]+ a + a ;
	return a ;
}





void init() {
	int a[deg], b[deg], c[2*deg-1] ;
	int i,j,k ;
	for( i = 0 ; i < pwr; i++ ) {
		binaryform( a, i ) ;
		mult( c, a, a ) ;
		mul[i][i] = eval(c) ;
		for (j = i+1 ; j < pwr; j++ ) {
			binaryform( a, i ) ;
			binaryform( b, j ) ;
			mult( c, a, b ) ;
			mul[i][j] = mul[j][i] = eval( c ) ;
		}
	}

	inv[0] = 0 ;
	for ( i = 1; i < pwr; i++ ) 
	{
		k=0 ;
		for ( j = 1 ; j < pwr ; j++ ) {
			if ( mul[i][j] == 1 ) {
				inv[i] = j ;
				if ( k == 1 ) cout << "error";
				k = 1 ;
			}
		}
	}

	
	for (i=0; i < deg; i++ ) a[i] = b[i] = 0 ;
	a[0] = b[1] = 1 ;
	for ( i = 0; i < pwrm1 ; i++ ) {
		j = eval(a) ;
		lg[j] = i ;   // logarithm
		ex[i] = j ;   // exponential
		mult( c, a, b ) ;
		for (j =0; j < deg; j++) a[j]=c[j] ;
	}
	ex[pwrm1] = 1;
}

istream& operator>> (istream& in,  GFpow& v){ 
	in >> v.a ;
	return (in ) ;
}	
ostream& operator<< (ostream& out, const GFpow& v){ 
	out << v.a ;
	return ( out ) ;
}
