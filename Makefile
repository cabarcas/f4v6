TLIB = -lstdc++
#INCLUDES = -I. -I$(BASEDIR)
CXX = gcc
#CXXFLAGS = -g $(INCLUDES)
DEPS = f4v6.h GFpow.h list_poly.h monomial.h output.h sp.h
SRC = GFpow.cc sp.cc sp_gauss.cc monomial.cc monomial_static.cc \
	list_poly.cc list_poly_io.cc f4v6.cc f4v6_test.cc main.cc
OBJ = $(addsuffix .o, $(basename $(SRC)))

.o: .c $(DEPS)
	$(CXX) -c -o $@ $<

f4v6: $(OBJ)
	$(CXX) -o $@ $^ $(TLIB)
