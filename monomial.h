/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file monomial.h
    Declaration of the monomial class functions.
*/
#ifndef MONOMIAL_H
#define MONOMIAL_H

#include<vector>
#include<map>
#include "util.h"
using namespace std;

/// A Monomial is a map from variables into exponents
/** Monomial t;
    t[2] = 3;
    t[7] = 1;
    creates the monomial x2^3 * x7
*/
class Monomial : public map<short,short>
{
public:
    /// An Index represents a monomial indexed in indx_monomials
    typedef int Index;
    //Conversion functions (monomial_static.cpp)
    Index to_index();                      ///< returns the corresponding index, or -1 if not indexed
    friend Monomial* to_monomial(const Index t);   ///< returns a pointer to the monomial indexed as t
    //Static functions (monomial_static.cpp)
    static void set_num_var(const short n);         ///< sets the number of variables to n
    static void increase_indexed(const short deg);  ///< indexes monomials up to degree deg
    static Index max_monomial(const short deg);     ///< Maximum indexed monomials of degree deg
    //divisor generation (monomial_static.cpp)
    friend void first_divisor(const Monomial &t, Monomial *u, short *d);
    friend bool next_divisor(const Monomial &t, Monomial *u, short *d);
    //basic operations (monomial.cpp)
    bool operator==(const Index t2) const;  ///< true if t1 == t2
    bool operator!=(const Index t2) const;  ///< true if t1 != t2
    friend short degree(const Index t);     ///< Degree of the monomial t
    friend short degree(const Monomial &t); ///< Degree of the monomial t
    friend Monomial mult(const Monomial &t1, const Monomial &t2);   ///< t1*t2 as a Monomial
    friend Monomial mult(const Monomial &t1, const Index t2);       ///< t1*t2 as a Monomial
    friend Monomial mult(const Index t1, const Index t2);           ///< t1*t2 as a Monomial
    friend Index mult_index(const Index t1, const Index t2);        ///< Index of t1*t2
    friend Index mult_index(const Monomial &t1, const Index t2);    ///< Index of t1*t2
    friend Monomial quo(const Monomial &t1, const Monomial &t2);///< t1/t2. Assumes t2 divides t1
    friend Monomial quo(const Monomial &t1, const Index t2);    ///< t1/t2. Assumes t2 divides t1
    friend Monomial quo(const Index t1, const Monomial &t2);    ///< t1/t2. Assumes t2 divides t1
    friend Monomial quo(const Index t1, const Index t2);        ///< t1/t2. Assumes t2 divides t1
    friend Index quo_index(const Monomial &t1, const Monomial &t2); ///< t1/t2 if t2 divides t1, -1 otherwise
    friend Index quo_index(const Index t1, const Monomial &t2);     ///< t1/t2 if t2 divides t1, -1 otherwise
    friend Index quo_index(const Index t1, const Index t2);         ///< t1/t2 if t2 divides t1, -1 otherwise
    friend bool does_divide(const Monomial &t1, const Monomial &t2);///< true if t1 divides t2
    friend bool does_divide(const Index t1, const Monomial &t2);    ///< true if t1 divides t2
    friend bool does_divide(const Index t1, const Index t2);        ///< true if t1 divides t2
    friend Monomial lcm(const Monomial &t1, const Monomial &t2);///< Least comon multiple of t1 and t2
    friend Monomial lcm(const Index t1, const Index t2);        ///< Least comon multiple of t1 and t2
    friend Index lcm_index(const Monomial &t1, const Index t2); ///< Index of least comon multiple of t1 and t2.
    friend Index lcm_index(const Index t1, const Index t2);     ///< Index of least comon multiple of t1 and t2.
    friend bool are_disjoint(const Monomial &t1, const Monomial &t2);///< True if t1 and t2 are disjoint
    friend bool are_disjoint(const Index t1, const Index t2);        ///< True if t1 and t2 are disjoint
private:
    //Static members (defined in ter_static.cpp)
    static short num_var;   ///< Number of variables
    static short cur_deg;   ///< maximum degree indexed so far
    /// Indexed monomials.
    /**All monomials up to certain degree in ascending order acording to the given monomial order*/
    static vector<Monomial> indx_monomials; 
    /// multiplication table.
    /** For indexed monomials up to degree cur_deg-1 gives the index of the monomial
        that results from multipliying by a variables x_i, i.e.
        x_i * indx_monomials[j] = indx_monomials[mult_table[i + num_var*j]]
    */
    static vector<Index> mult_table;
    /// Degree separations on indx_monomials. deg_sep[d] is the last index of a monomial of degree d.
    static vector<Index> deg_sep;
    //private functions
    friend short compare(const Monomial &t1, const Monomial &t2);   ///< Compares t1 and t2 in glex
};
ostream& operator<<(ostream& out, const Monomial &t); ///< Outputs t to a stream
//----------------------------------------------------------------------------------
//----------------------------------Inline Implementations--------------------------
//----------------------------------------------------------------------------------
/// sets the number of variables to n
inline void Monomial::set_num_var(const short n)
{
    Monomial::num_var = n;
}
/// returns a pointer to the monomial indexed as t
inline Monomial* to_monomial(const Monomial::Index t)
{
    return &Monomial::indx_monomials[t];
}
/// Maximum indexed monomials of degree deg
/** \return If deg <= cur_deg returns the index of the maximum monomial of degree deg indexed.
    otherwise, return -1.
*/
inline Monomial::Index Monomial::max_monomial(const short deg)
{
    return cur_deg >= deg ? deg_sep[deg] : -1;
}
/// Returns true if t1 == t2
inline bool Monomial::operator==(const Monomial::Index t2) const
{
    return *this == indx_monomials[t2];
}
/// Returns true if t1 == t2
inline bool Monomial::operator!=(const Monomial::Index t2) const
{
    return *this != indx_monomials[t2];
}
/// Returns t1*t2 as a Monomial
inline Monomial mult(const Monomial &t1, const Monomial::Index t2)
{
    return mult(t1, Monomial::indx_monomials[t2]);
}
/// Returns t1*t2 as a Monomial
inline Monomial mult(const Monomial::Index t1, const Monomial::Index t2)
{
    return mult(Monomial::indx_monomials[t1], Monomial::indx_monomials[t2]);
}
/// Returns t1*t2 as and int
inline Monomial::Index mult_index(const Monomial::Index t1, const Monomial::Index t2)
{
    return mult_index(Monomial::indx_monomials[t1], t2);
}
/// Returns t1/t2. Assumes t2 divides t1.
inline Monomial quo(const Monomial &t1, const Monomial::Index t2)
{
    return quo(t1, Monomial::indx_monomials[t2]);
}
/// Returns t1/t2. Assumes t2 divides t1.
inline Monomial quo(const Monomial::Index t1, const Monomial &t2)
{
    return quo(Monomial::indx_monomials[t1], t2);
}
/// Returns t1/t2. Assumes t2 divides t1.
inline Monomial quo(const Monomial::Index t1, const Monomial::Index t2)
{
    return quo(Monomial::indx_monomials[t1], Monomial::indx_monomials[t2]);
}
/// Returns -1 if t2 does not divide t1, t1/t2 otherwise
inline Monomial::Index quo_index(const Monomial::Index t1, const Monomial &t2)
{
    return quo_index(Monomial::indx_monomials[t1], t2);
}
/// Returns -1 if t2 does not divide t1, t1/t2 otherwise
inline Monomial::Index quo_index(const Monomial::Index t1, const Monomial::Index t2)
{
    return quo_index(Monomial::indx_monomials[t1], Monomial::indx_monomials[t2]);
}
/// Returns true if t1 divides t2
inline bool does_divide(const Monomial::Index t1, const Monomial &t2)
{
    return does_divide(Monomial::indx_monomials[t1], t2);
}
/// Returns true if t1 divides t2
inline bool does_divide(const Monomial::Index t1, const Monomial::Index t2)
{
    return does_divide(Monomial::indx_monomials[t1], Monomial::indx_monomials[t2]);
}
/// finds the least comon multiple of t1 and t2
inline Monomial lcm(const Monomial::Index t1, const Monomial::Index t2)
{
    return lcm(Monomial::indx_monomials[t1], Monomial::indx_monomials[t2]);
}
/// finds the least comon multiple of t1 and t2
/** Only use if it is known that the result's deg is <= cur_def
*/
inline Monomial::Index lcm_index(const Monomial::Index t1, const Monomial::Index t2)
{
    return lcm_index(Monomial::indx_monomials[t1], t2);
}
/// Returns true if the monomials t1 and t2 are disjoint
inline bool are_disjoint(const Monomial::Index t1, const Monomial::Index t2)
{
    return are_disjoint(Monomial::indx_monomials[t1], Monomial::indx_monomials[t2]);
}
#endif
