/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file list_poly.h
    Declaration of the List_poly class.    
*/
#ifndef LIST_POLY_H
#define LIST_POLY_H

#include"monomial.h"
#include"sp.h"

typedef Sp Matrix;  ///< Sparse matrix structure
/// Implements a list of polynomials
class List_poly
{
public:
    //Construction & destructions
    List_poly(const short d = 2);   ///< Initializes The list with maximum degree d
    List_poly(const List_poly &F);  ///< Copy constructor
    void reset(const short d);      ///< Increases the degree allowed to be stored
    void clear();                   ///< Erases all elements and releases memory
    //Access Functions
    short degree() const;   ///< Maximum degree of the polynomials
    int size()const;        ///< Number of polynomials
    vector<Monomial::Index>::const_iterator lead_monomial_begin()const; ///< Iterator pointing to the begining of a sequence of the leading monomials.
    vector<Monomial::Index>::const_iterator lead_monomial_end()const;   ///< Iterator pointing to the end of a sequence of the leading monomials.
    Monomial::Index lead_monomial(const int i_f)const;  ///< Leading monomial of the i-th poly
    //functions to modify the list
    int new_poly(const int n = 1);      ///< Inserts n zero polynomials at the end and returns the position of the first inserted.
    void remove_poly(const int i_rem);  ///< Removes i-th poly.
    //Predicates
    bool all_zero_coefs(const Monomial::Index t)const;  ///< True if for all polys the coef of t is zero, false otherwise.
    int nnz_polys() const;    ///< Returns the number of non-zero polynomials
    int nnz_monomials() const;    ///< Returns the number of non-zero monomials
    int nnz_coefs() const;    ///< Returns the number of non-zero coeficients
    //other functions
    friend void update_lead_monomial(List_poly *H);  ///< Updates the leading monomials for the elements in H
    friend void reduction_row_echelon(List_poly *H); ///< Reduces H to row echelon form and updates its leading monomials
    friend int find_LM(const List_poly &G, const Monomial::Index t); ///< Searches a poly in G with leading monomial t and returns its index or -1 if not found.
    friend void multiply(const Monomial::Index t, const List_poly &G, int i_g, List_poly *H, const int i_h);///< Multiplies the poly in G[i_f] by the monomial t and stores the result in H[i_h]
    friend void multiply(const Monomial &t, const List_poly &G, int i_g, List_poly *H, const int i_h);      ///< Multiplies the poly in G[i_f] by the monomial t and stores the result in H[i_h]
    friend class col_transform; ///< Class function that transforms columns between 2 List_poly
private:
    //Data members
    short deg;  ///< maximun degree
    int m_poly; ///< Number of polynomials
    /** A matrix of size m*n where n = comb(num_var+d, d) = Monomial::max_monomial(deg)+1.
        For 0 <= i <= m, 0 <= j <= n, 
        coef[i][j] gives the coeficient of the i-th polynomial in the col_to_monomial(n-j) monomial
    */
    Matrix coef;            
    vector<Monomial::Index> lead_t;     ///< lead_t[i] gives the leading monomial of the i-th polynomial
    //Private functions for basic access
    void set_coef(  const int i_f,
                    const Monomial::Index t,
                    const Field v); ///< Sets the coeficient of the monomial t of the i-th poly to v. It is assumed that it doesn't contain such monomial.
    int max_monomial() const;       ///< maximum monomial up to this degree
    int monomial_to_col(const Monomial::Index t) const; ///< Converts an index monomial into column
    Monomial::Index col_to_monomial(const int j) const; ///< Converts a column into an index monomial 
    //input-output
    friend void read_magma(const char *file_name, List_poly &F);    ///< Reads from a file in a specific formar. (see "input format.txt")
    friend ostream& operator<<(ostream& out, const List_poly &F);   ///< Outputs to a stream
    friend void print_ncsry(ostream& out, const List_poly &F, vector<bool>& ncsry); ///< outputs to a stream only in ncsry[i] is true
};
//---------------------------------------------------------------------------------------
//-----------------------------------Access Functions Inline Implementation--------------
//---------------------------------------------------------------------------------------
/// Maximum degree of the polynomials
inline short List_poly::degree() const
{
    return this->deg;
}
/// Number of polynomials
inline int List_poly::size()const
{
    return this->m_poly;
}
/// Iterator pointing to the begining of a sequence of the leading monomials.
inline vector<Monomial::Index>::const_iterator List_poly::lead_monomial_begin()const
{
    return this->lead_t.begin();
}
/// Iterator pointing to the end of a sequence of the leading monomials.
inline vector<Monomial::Index>::const_iterator List_poly::lead_monomial_end()const
{
    return this->lead_t.end();
}
/// Leading monomial of the i-th poly
inline Monomial::Index List_poly::lead_monomial(const int i_f)const
{
    return this->lead_t[i_f];
}
//---------------------------------------------------------------------------------------
//-----------------------------------Predicates Inline Implementation--------------------
//---------------------------------------------------------------------------------------
/// True if for all polys the coef of t is zero, false otherwise.
inline bool List_poly::all_zero_coefs(const Monomial::Index t)const
{
    return this->coef.is_zero_col(this->monomial_to_col(t));
}
/// Returns the number of non-zero polynomials
inline int List_poly::nnz_polys() const
{
    return this->coef.nnz_rows();
}
/// Returns the number of non-zero monomials
inline int List_poly::nnz_monomials() const
{
    return this->coef.nnz_cols();
}
/// Returns the number of non-zero coeficients
inline int List_poly::nnz_coefs() const
{
    return this->coef.nnz();
}
//---------------------------------------------------------------------------------------
//-----------------------------------Private functions for basic access------------------
//---------------------------------------------------------------------------------------
/// Sets the coeficient of the monomial t of the i-th poly to v. It is assumed that it doesn't contain such monomial.
inline void List_poly::set_coef(const int i_f, const Monomial::Index t, const Field v)
{
    this->coef.entry( i_f, monomial_to_col(t), v );
}
/// Number of monomials up to this degree
inline int List_poly::max_monomial() const
{
    return Monomial::max_monomial(deg);
}
/// Converts an index monomial into an index for a column
inline int List_poly::monomial_to_col(const Monomial::Index t) const
{
    return max_monomial() - t;
}
/// Converts an index for a column into an index monomial 
inline Monomial::Index List_poly::col_to_monomial(const int j) const
{
    return max_monomial() - j;
}
//---------------------------------------------------------------------------------------
//-----------------------------------other functions-------------------------------------
//---------------------------------------------------------------------------------------
/// Multiplies the poly in G[i] by the monomial t and stores the result in H[i_h]
inline void multiply(const Monomial::Index t, const List_poly &G, int i_g, List_poly *H, const int i_h)
{
    multiply(*(to_monomial(t)), G, i_g, H, i_h);
}
#endif
