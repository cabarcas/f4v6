/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file sp.h
    Declaration of the sp class.
*/
#ifndef SP_H
#define SP_H
#include "util.h"
#include "GFpow.h"

typedef GFpow Field;    ///< Base field for entries

//A sparse matrix
class Sp
{
public:
    // Constructors
    Sp();
    Sp(int m, int n, int nzmax);
    void reset(int m, int n, int nzmax);
    void reset_skim(int m, int n, int nzmax);
    Sp(const Sp &A);
    const Sp &operator=(const Sp &A);
    ~Sp();
    void clear();
    // Functions
    void entry(int i, int j, Field x);///< add an entry to a triplet matrix; return 1 if ok, 0 otherwise 
    void remove_row(int i);   ///< deletes row i
    int purge();      ///< removes all zero rows and zero entries in front of first non-zero of each row. Returns the number of rows left
    void reserve_rows(int m1);
    // Predicates
    bool is_zero_col(int j) const;
    int first_non_zero(int i) const;  ///< Returns the column of the first non_zero entry of row i
    int n_cols() const {return n;};
    int nnz_rows() const;   ///< Returns the number of non-zero rows
    int nnz_cols() const;   ///< Returns the number of non-zero columns
    int nnz() const;        ///< Returns the number of non-zero entries
    // Other functions
    friend int gauss(Sp &A, int *pivot_col, double threshold_density = -1);    ///< Gaussian elimination
    //friend int gauss(Sp &A, int *pivot_col, int m_real, double min_col_sparse_percent);
    template <class transform_col_fnc>
        friend void copy_transform(const Sp &A, int i_A, Sp &B, int i_B, transform_col_fnc fnc); ///< copies row i_A of A into row i_B of B transforming cols of A into cols of B using fnc
    friend int full_size_row(const Sp &A, int i, Field *aux);
private:
    // Private functions
    void stretch(int nzmax);
    void remove_entry(int k, int pre_k_col = -2);    
    int addrow(int i1, int i2, const Field c, Field *full_vec);
// Member Data
    int nzmax ;  // maximum number of nonzero entries
	int nz;  //actual number of nonzero entries
    int m ;  // number of rows 
    int n ;  // number of columns
	Field *val;  //numerical value of entry i (size nzmax)
	int *irowst;  //location of first entry for row i (size m)
	int *j;  //column number of entry i (size nzmax)
	int *linkrw; //link to the next entry in the row of entry i, or -1 if last (size nzmax). Rows are kept in order
	int *jcolst;  //location of first entry for col i(size n)
	int *i;  //row number of entry i (size nzmax)
	int *linkcl;  //link to the next entry in the col of entry i, or -1 if last (size nzmax). Cols are NOT kept in order
	int *del;  //deteted elements (elts that became zero)
	int maxdel;  //maximun number of deleted elements
	int ndel;  //current number of deleted elts
};

template <class transform_col_fnc>
inline void copy_transform(const Sp &A, int i_A, Sp &B, int i_B, transform_col_fnc fnc)
{
    for(int k = A.irowst[i_A]; k >= 0; k = A.linkrw[k])
        B.entry(i_B, fnc(A.j[k]), A.val[k]);
}


#endif //SPARSE_H
