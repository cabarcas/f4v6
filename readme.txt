Program: f4 version 6
By: Daniel Cabarcas. cabarcas@gmail.com

Part of: "An Implementation of Faug�re's F4 Algorithm for Computing Gr�bner Bases".

A thesis submitted to the Graduate School of the University of Cincinnati
in partial fulfillment of the requirements for the degree of Master of Science
in the Department of Computer Science
of the College of Engineering
May 2010

Committee Chair: Prof. Dr. Dieter Schmidt

http://etd.ohiolink.edu/view.cgi?acc_num=ucin1277120935