/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file sp_gauss.cpp
    Implementation of gaussian eluimination functions for the sp class.
*/
#include "sp.h"
#include "output.h"

///A(i1) = A(i1) + c*A(i2), where A(i) means row i of A
/** Returns the number of fill ins
*/
int Sp::addrow(int i1, int i2, const Field c, Field *full_vec)
{
    int fill_ins = 0;
    int j;
    int k1;
    //load row i2 into full_vec
    int k2;
    for(k2 = this->irowst[i2]; k2 >= 0; k2 = this->linkrw[k2])
        full_vec[this->j[k2]] = c*this->val[k2];
    //Scan row i1
    for(k1 = this->irowst[i1]; k1 >= 0; k1 = this->linkrw[k1])//for each entry in row i1
    {
        //check corresponding entry in full_vec
        j = this->j[k1];
        this->val[k1] += full_vec[j];
        full_vec[j] = 0;
    }
    //Scan i2
    for(k2 = this->irowst[i2]; k2 >= 0; k2 = this->linkrw[k2])
    {
        //check corresponding entry in full_vec
        j = this->j[k2];
        if(full_vec[j] != 0)
        {
            this->entry(i1, j, full_vec[j]);
            fill_ins++;
            full_vec[j] = 0;//to reuse vector
        }
    }
	return fill_ins;
}
///A(i1) = A(i1) + c*A(i2), where A(i) means row i of A
void add_row_dense(Field *A, int n, int j_start, int i1, int i2, const Field c)
{
    for(int j = j_start; j < n; j++)
        A[i1*n + j] += c*A[i2*n + j];
}
int gauss(Sp &A, int *pivot_col, double threshold_density)
{//zeros below and above pivots. choose pivot by min nnz entries in row
    //Before we start lets compactify the matrix --------
    A.stretch(A.nz);
    int i = A.m-1;
    while(i >= 0 && A.irowst[i] == -1)
        i--;
    A.m = i+1;
    A.irowst = (int *)realloc(A.irowst, A.m* sizeof(int));
    //---------------------------------------------------
	int rank = 0;
    int j;
    //auxiliary full-length-vector
    Field *full_vec = (Field *)calloc(A.n, sizeof(Field));
    //initialize full_vec to zero
    for(j = 0; j < A.n; j++)
        full_vec[j] = 0;
    //--------- Initialize dense columns
    bool *is_dense = (bool*)calloc(A.n, sizeof(bool));
    for(j = 0; j < A.n; j++)
        is_dense[j] = false;
    int *map_dense_col = (int *)calloc(A.n, sizeof(int));
    for(j = 0; j < A.n; j++)
        map_dense_col[j] = -1;
    if(threshold_density == -1) //< If threshold_density not given.
    {
        //y = 0.0775x - 1.1794
        threshold_density = (double)A.nnz() / A.nnz_cols() / A.nnz_rows() * 100;
        threshold_density = .077508*threshold_density - 1.1794;
        if(threshold_density < .01)
            threshold_density = .01;
        if(threshold_density > .2)
            threshold_density = .2;
    }
    int threshold = (int)(A.m * threshold_density);
    int n_dense = 0;
    for(int j = 0; j < A.n; j++)//for each column
    {
        int nnzcol = 0;  //zero initialy
        for(int k = A.jcolst[j]; k >= 0; k = A.linkcl[k])
	        nnzcol++;
        if(nnzcol > threshold)
        {
            is_dense[j] = true;
            n_dense++;
        }
    }
#ifdef PRINT_MEMORY
    fout<<'\t'<<n_dense;
#endif
    Field *dense_matrix = (Field *)calloc(A.m * n_dense, sizeof(Field));
    for(int k = 0; k < A.m * n_dense; k++)
        dense_matrix[k] = 0;
    //fill dense matrix
    int *map_sparse_col = (int *)calloc(n_dense, sizeof(int));
    int j_dense = 0;
    for(int j = 0; j < A.n; j++)//for each column
    {
        if(is_dense[j])
        {
            map_sparse_col[j_dense] = j;
            map_dense_col[j] = j_dense;
            for(int k = A.jcolst[j]; k >= 0; k = A.linkcl[k])
            {
                dense_matrix[A.i[k] * n_dense + j_dense] = A.val[k];                
                A.remove_entry(k, -1);
            }
            j_dense++;
        }
    }
    //----------------------------------
    bool *active_row = (bool*)calloc(A.m, sizeof(bool));    //< is an active row?
    for(i = 0; i < A.m; i++)
        active_row[i] = true;

   	int *nnzrow = (int*)calloc(A.m, sizeof(int));   //< number of non-zero entries in sparse columns
    //fill nnzrow 
	for(i = 0; i < A.m; i++)
	{
		nnzrow[i] = 0;  //zero initialy
		int k;  //k-th entry in non-zero vector
		for(k = A.irowst[i]; k >= 0; k = A.linkrw[k])
			nnzrow[i]++;
	}
    j_dense = 0;
	for(j = 0; j < A.n; )//for each column
	{
        //look through the current column for an active row with nonzero entry and lowest nnzrow
		int pivot_k = -1;   //best pivot. k values index the non-zero vector of A
        int pr = -1;        //pivotal row
        int best_nnzrow = A.n + 1;
        if(!is_dense[j])
        {            
		    for(int aux_k = A.jcolst[j], pre_aux_k = -1; aux_k >= 0; aux_k = A.linkcl[aux_k])
            {
                pre_aux_k = aux_k;
                if(active_row[A.i[aux_k]] &&    //< an active row
                    A.val[aux_k] != 0 &&        //< with nonzero entry
                    nnzrow[A.i[aux_k]] < best_nnzrow)//lower nnzrow
                {
                    pivot_k = aux_k;
                    best_nnzrow = nnzrow[A.i[aux_k]];
                }
            }
        }
        else
        {
            j_dense = map_dense_col[j];
		    for(int i = 0; i < A.m; i++)
            {
                if(active_row[i] &&                             //< an active row 
                    dense_matrix[i*n_dense + j_dense] != 0 &&   //< with nonzero entry
                    nnzrow[i] < best_nnzrow)                    //< lower nnzrow
                {
                    pr = i;
                    best_nnzrow = nnzrow[i];
                }
            }			
        }
        //if there is an active row with non zero j entry. Otherwise the colum j in done
		if((!is_dense[j] && pivot_k >= 0) || (is_dense[j] && pr >= 0)) 
        {
			rank++;
            Field pv;
            if(!is_dense[j])
            {
			    pr = A.i[pivot_k]; //pivotal row
			    pv = A.val[pivot_k]; //pivotal value
            }
            else
                pv = dense_matrix[pr*n_dense + j_dense];
            pivot_col[pr] = j;  //< save info about pivot column
            active_row[pr] = false; //pivoral row is not active anymore, it is done
            if(!is_dense[j])
            {
    			//for each row different from the pivotal row with a nonzero entry in the current column
                int k,  //index in the non-zero vector of A of the next entry in the pivotal column
                    pre_k;  //points at the previous entry in the column
                for(k = A.jcolst[j], pre_k = -1; k >= 0; k = A.linkcl[k])
			    {
			        i = A.i[k];    //row to be eliminated
			        if(i != pr) //if the row is different from the pivotal row
				    {
                        Field c = A.val[k];
					    if(c != 0)
                        {
                            int fill_ins = A.addrow(i, pr, -c / pv, full_vec);
                            if(active_row[i]) //if the row is active
                                nnzrow[i] += fill_ins;
                            add_row_dense(dense_matrix, n_dense, j_dense ,i, pr, -c / pv);
                        }
					    //remove entry i,j known to be 0
                        if(active_row[i]) //if the row is active
                            nnzrow[i]--;//substract one to nnzrow[i]
                        A.remove_entry(k, pre_k);
				    }//if i != pr
				    else
					    pre_k = k;
			    }//for k
            }
            else
            {
                //for each row different from the pivotal row with a nonzero entry in the current column
                for(int i = 0; i < A.m; i++)
                {
                    //if the row is different from the pivotal row
                    if(i != pr)
                    {
                        //if the entry is non-zero
                        Field c = dense_matrix[i*n_dense + j_dense];
                        if(c != 0)
                        {
                            int fill_ins = A.addrow(i, pr, -c / pv, full_vec);
                            if(active_row[i]) //if the row is active
                                nnzrow[i] += fill_ins;
                            add_row_dense(dense_matrix, n_dense, j_dense, i, pr, -c / pv);
                        }
                    }
                }
            }
		}//if there is active row
        int h = j+1;
        j = h;
	}//j loop
    free(full_vec);
	free(nnzrow);
    free(is_dense);
    free(map_dense_col);
    free(active_row);
    //------------------- copy dense into sparse matrix
    for(int i = 0; i < A.m; i++)
    {
        for(int j_dense = 0; j_dense < n_dense; j_dense++)//for each column
        {
            Field c = dense_matrix[i*n_dense + j_dense];
            if(c != 0)
                A.entry(i, map_sparse_col[j_dense], c);
        }
    }
 #ifdef PRINT_MEMORY
    PROCESS_MEMORY_COUNTERS pmc;
    if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
        fout<<'\t'<< setprecision( 4 )<<(double)pmc.WorkingSetSize / 1000000;
#endif
   //-------------------------------------------------
    free(dense_matrix);
    free(map_sparse_col);
    return rank;
}
